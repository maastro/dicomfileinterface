# CODING GUIDELINES #

This file provides guidelines to contribute to the library. These are included to maintain the quality of the library that we are aiming for. Please contact us if you have any problems or questions

### DESIGN ###
Coding in Matlab can be done in many ways. The object oriented structure provides ample tools to write *clean code*. For this library a [functional design](https://en.wikipedia.org/wiki/Functional_programming "functional design") is implemented. In short this means that **data** is modeled in (value) classes and **data transformations** are modeled in functions.

### NAMING CONVENTIONS ###
1. Functions start with a small letter
2. Classes start with a capital letter
3. Instances of classes start with a small letter
4. CamelCasing is used for all naming purposes
5. ALL_CAPS is used for constants
6. Add *"help"* documentation for every public function

### ERROR HANDLING ###
1. Public functions should verify the input and throw exceptions for improper calls of the function. (private functions that are internal for the library do not have to be checked because the library always controls the in and output of these functions.);
2. When throwing an exception use the following format for the message identifier (msgid): *"function:subfunction:errorIdentifier"* (available errorIdentifiers are elaborated at the end of this chapter);
3. The public functions should always have proper error handling that catches any errors the underlying functions may throw. (see logging)

#### ERROR IDENTIFIERS ####
1. InvalidInput: If the user provides an input that is not supported by the public function;
2. FileNotFound: If a file or folder is not available;
3. DataNotAvailable: If the data requested is not loaded into memory (for instance, a DicomObj in the DicomDatabase class);
4. ProcessingError: If a function throws an unhandled exception when trying to perform a calculation;

### LOGGING ###
The library uses a public available logging framework that is provided on github (see submodules). Please use the following guidelines when added code to the repository.

1. Use **error** logging when unhandled exceptions occur;
2. Use **warn** logging to show when results are not generated but the code can still continue;
3. Use **info** logging to show important data transitions / results;
4. Use **debug** logging to show internal data / steps that can help the admins pinpoint where any problems may occur. The matlab function *getReport* will print details of the exception;
5. **Trace** level logging is not implemented;

