# README #

To use this Matlab library no further installation is required. All necessary dependencies are included with git sub-modules. This library was written for Matlab 2015b and was tested for this version. Earlier versions will issues reading the DICOM files. Newer versions should be verified by the user.


### What is this repository for? ###

* Reading DICOM RT files into memory
* Sorting DICOM RT files using the referencing structure
* Creating 3D bitmasks of delineations in the RTSTRUCT
* Combining delineations in the RTSTRUCT into one 3D bitmask
* Creating 3D image data for delineations in the RTSTRUCT

### How do I get set up? ###

* Clone/pull/download the latest release of the repository
* Install/update the dependencies using the [Matlab dependency manager](https://bitbucket.org/maastroclinic/matlab-dependency-manager.git). Alternatively all dependencies in setup.m can be downloaded/cloned manually.
* run **"setPathForUse.m"**

### How do I verify my library? ###
* download the DICOM data accompanied with this repository (see downloads)
* unzip the DICOM data in the resources folder
* run **"setPathForTesting.m"**
* run **"fullSystemTest.m"**
* if all tests are successful the library works for your system and it is ready for use! 

### Contribution guidelines ###

* Use the *prototype* folders to save functions that are not verified/tested but can be useful for others
* Please fork this repository when wanting to make adjustments, pull request with changes/bugfixes are welcome!
* To keep the code readable and similar please consult the **codingStandards.md** included with this repository when contributing 

### Who do I talk to? ###

* MAASTRO Clinic Software Development
* Tim Lustberg 
	* tim.lustberg@maastro.nl