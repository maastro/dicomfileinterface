function list = setup()
    list(1) = DependencyContainer('https://github.com/TimLustberg/jsonlab');
    list(2) = DependencyContainer('https://github.com/TimLustberg/logging4matlab');
    list(3) = DependencyContainer('https://bitbucket.org/maastroclinic/struct-utilities.git');
end