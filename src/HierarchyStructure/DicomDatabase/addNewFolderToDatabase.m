function dicomDb = addNewFolderToDatabase(dicomDb, folder)
% ADDNEWFOLDERTODATABASE is a customized version of parseDicomObjFromFilesInFolder to only read
% valid Dicom files which have not been added before
%
% dicomDb = addNewFolderToDatabase(dicomDb, folder)
%
% See also: DICOMDATABASE, DICOMOBJ

    if ~isdir(folder)
        throw(MException('addNewFolderToDatabase:InvalidInput', 'provided folder is not a valid folder'))
    end
    logger = logging.getLogger('dicom-file-interface');
    logger.info(['Reading dicom files of folder (' folder ') and add them to DicomDatabase ']);

    files = rdir(fullfile(folder, '**', '*'));
    nrOfFiles = length(files);
    logger.info(['number of files found: ' num2str(nrOfFiles)]);
    for file = files'
        dicomDb = addFileToDatabase(dicomDb, file, logger);
    end
end

function dicomDb = addFileToDatabase(dicomDb, file, logger)
    if dicomDb.fileAvailableInDb(file)
        logger.debug(['skipping file (' file.name ') because it is already loaded']);
        return;
    end

    if ~isdicom(file.name)
        logging.debug(['skipping file (' file.name ') because it is not a valid dicom file'])
        return;
    end
        
    try
        dicomObj = DicomObj(file.name);
        dicomDb = dicomDb.parseDicomObj(dicomObj);
        logger.debug(['added file(' file.name ') to the DicomDatabase' ])
    catch exception
        logger.error(['skipping file. exception while reading file: ' file.name]);
        logger.error(getReport(exception, 'extended', 'hyperlinks', 'off'));
    end
end