classdef PlanReferenceObjects
    %PLANREFERENCEOBJECTS is a value class to store ReferenceUidSets for a Patient object in die
    %HierarchyStructure of DICOM.
    %
    % See also: REFERENCEUIDSET, PATIENT, DICOMOBJ
    
    properties
        planUids
        planLabels
        labelsForPlan
        rtDosesForPlan
        rtImagesForPlan
        rtStructsForPlan
        ctSeriesForStruct
        refUids
    end
    
    methods
        function this = PlanReferenceObjects()
            this = this.createMappingStructures();
        end
        
        function this = parseDicomObject(this, dicomObj)
        %PARSEDICOMOBJ(dicomObj) parses the provided dicomObj based on the modality to create
        % references between objects on patient level
            logger = logging.getLogger('dicom-file-interface');
            logger.warn('deprecated function, please use parseDicomObj, will be removed in future release');
            this = this.parseDicomObj(dicomObj);
        end
        
        function this = parseDicomObj(this, dicomObj)
        %PARSEDICOMOBJ(dicomObj) parses the provided dicomObj based on the modality to create
        % references between objects on patient level
            this.refUids(dicomObj.sopInstanceUid) = ReferenceUidSet(dicomObj);
            
            modalityObj = createModalityObj(dicomObj);
            switch modalityObj.modality
                case 'rtplan'
                    this = this.addRtPlan(modalityObj);
                case 'rtstruct'
                    this = this.addRtStruct(modalityObj);
                case 'rtdose'
                    this = this.addRtDose(modalityObj);
                case 'rtimage'
                    this = this.addRtImage(modalityObj);
            end
        end
        
        function out = get.planUids(this)
            out = this.labelsForPlan.keys;
        end
        
        function out = get.planLabels(this)
            out = this.labelsForPlan.values;
        end
        
        function uid = getPlanUidForLabel(this, label)
            uid = [];
            keys = this.labelsForPlan.keys;
            for i = 1:this.labelsForPlan.Count
                if strcmp(label, this.labelsForPlan(keys{i}))
                    uid = keys{i};
                    break;
                end
            end
        end
    end
    
    methods (Access = 'private')
        function this = createMappingStructures(this)
            this.rtDosesForPlan = containers.Map;
            this.rtImagesForPlan = containers.Map;
            this.labelsForPlan = containers.Map;
            this.rtStructsForPlan = containers.Map;
            this.ctSeriesForStruct = containers.Map;
            this.refUids = containers.Map;
        end
        
        function this = addRtPlan(this, rtPlan)
            this.labelsForPlan(rtPlan.sopInstanceUid) = rtPlan.planLabel;
            this.rtStructsForPlan(rtPlan.sopInstanceUid) = rtPlan.rtStructReferenceUid;
        end
        
        function this = addRtDose(this, rtDose)           
            if this.rtDosesForPlan.isKey(rtDose.referencedRtPlanUid)
                uidList = this.rtDosesForPlan(rtDose.referencedRtPlanUid);
                uidList(length(uidList)+1) = ReferenceUidSet(rtDose);
                this.rtDosesForPlan(rtDose.referencedRtPlanUid) = uidList;
            else
                this.rtDosesForPlan(rtDose.referencedRtPlanUid) = ReferenceUidSet(rtDose);
            end
        end
        
        function this = addRtStruct(this, rtStruct)
            this.ctSeriesForStruct(rtStruct.sopInstanceUid) = rtStruct.referencedImageSeriesUid;
        end
        
        function this = addRtImage(this, rtImage)
            if this.rtImagesForPlan.isKey(rtImage.referencedRtPlanUid)
                uidList = this.rtImagesForPlan(rtImage.referencedRtPlanUid);
                uidList(length(uidList)+1) = ReferenceUidSet(rtImage);
                this.rtImagesForPlan(rtImage.referencedRtPlanUid) = uidList;
            else
                this.rtImagesForPlan(rtImage.referencedRtPlanUid) = ReferenceUidSet(rtImage);
            end
        end
    end
end