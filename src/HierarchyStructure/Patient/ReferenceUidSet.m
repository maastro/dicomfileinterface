classdef ReferenceUidSet
    %REFERENCEUIDSET value class to hold the keys needed to reference the DicomObj instances
    % this enabled the referencing without keeping all DicomObjs in memory
    
    properties
        studyInstanceUid
        seriesInstanceUid
        sopInstanceUid
        filename
    end
    
    methods
        function this = ReferenceUidSet(dicomObj)
            logger = logging.getLogger('dicom-file-interface');
            if nargin ~= 1 || ~isa(dicomObj, 'DicomObj')
                throw(MException('ReferenceUidSet:constructor:InvalidInput', 'incorrect input provided, constructor needs a single DicomObj'));
            end
            if isempty(dicomObj.studyInstanceUid)
                throw(MException('ReferenceUidSet:constructor:InvalidInput', 'provided input is an empty DicomObj'));
            end
            
            this.studyInstanceUid = dicomObj.studyInstanceUid;
            this.seriesInstanceUid = dicomObj.seriesInstanceUid;
            this.sopInstanceUid = dicomObj.sopInstanceUid;
            this.filename = dicomObj.filename;
            
            logger.debug(['Filename          : ' this.filename]);
            logger.debug(['StudyInstanceUid  : ' this.studyInstanceUid]);
            logger.debug(['SeriesInstanceUid : ' this.seriesInstanceUid]);
            logger.debug(['SopInstanceUid    : ' this.sopInstanceUid]);
        end
    end
end