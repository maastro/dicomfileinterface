function [rtPlan, rtDose, rtStruct, ctScan] = createPlanPackage(patient, planLabel)
%CREATEPLANPACKAGE parses a patient object to collect all DicomObjs connected to the provided
%   planLabel and returns them if available.
%
%   [rtPlan, rtDose, rtStruct, ctScan] = createPlanPackage(patient, planLabel)
%
% see also: DICOMOBJ, PATIENT, DICOMDATABASE, RTPLAN, RTDOSE, RTSTRUCT, CTSCAN

    if ~isa(patient, 'Patient') || isempty(patient.id)
        throw(MException('createPlanPackage:InvalidInput', 'invalid patient object provided'))
    end

    planSopInstanceUid = patient.planReferenceObjects.getPlanUidForLabel(planLabel);
    if isempty(planSopInstanceUid)
        throw(MException('createPlanPackage:DataNotAvailable', 'planLabel not found for patient'))
    end

    try
        rtPlan = createModalityObj(patient.getDicomObj(planSopInstanceUid));
        rtDose = getRtDoseForPlan(patient, planSopInstanceUid);
        rtStruct = getRtStructForPlan(patient, planSopInstanceUid);
        ctScan = getCtScanForPlan(patient, planSopInstanceUid);
    catch exception
        logger = logging.getLogger('dicom-file-interface');
        logger.error(getReport(exception, 'extended', 'hyperlinks', 'off'));
        newException = MException('createPlanPackage:ProcessingError', ['could not create plan package for patient:' patient.id ' plan: ' planLabel]);
        newException = addCause(newException, exception);
        throw(newException);
    end
end