function ctScan = getCtScanForPlan(patient, planSopInstanceUid)
%GETCTSCANFORPLAN is part of a library of functions to find DICOM references.
%
% ctScan = getCtScanForPlan(patient, planUid) returns an CtScan object that is linked to the planSopInstanceUid
%   provided.
%
% See also: PATIENT, DICOMOBJ, CREATEPLANPACKAGE

    rtStructSopInstanceUid = patient.planReferenceObjects.rtStructsForPlan(planSopInstanceUid);
    if ~patient.planReferenceObjects.ctSeriesForStruct.isKey(rtStructSopInstanceUid)
        throw(MException('getCtScanForPlan:DataNotAvailable', ['the requested CtScan could not be returned because the RtStruct for plan ' planSopInstanceUid ' is missing because reference object is not complete']));
    end

    refUids = patient.planReferenceObjects.refUids(rtStructSopInstanceUid); %assume struct and ct are in same study
    study = patient.getStudyObject(refUids.studyInstanceUid);
    if study.isSeriesAvailable(patient.planReferenceObjects.ctSeriesForStruct(rtStructSopInstanceUid))
        series = study.getSeriesObject(patient.planReferenceObjects.ctSeriesForStruct(rtStructSopInstanceUid));
    else
        throw(MException('getCtScanForPlan:DataNotAvailable', ['the requested CtScan could not be returned because the CtScan for plan ' planSopInstanceUid ' is missing']));
    end

    ctScan = CtScan();
    ctScan = ctScan.addListOfObjects(series.getDicomObjArray);
end