function rtDose = getRtDoseForPlan(patient, planSopInstanceUid)
%GETRTDOSEFORPLAN is part of a library of functions to find DICOM references.
%
%rtDose = getRtDoseForPlan(patient, planSopInstanceUid) returns an RtDose object that is linked to the planSopInstanceUid
%   provided. only plan summation type RtDose object of supported TreatmentPlanning systems are
%   collected
%
% See also: PATIENT, DICOMOBJ, CREATEPLANPACKAGE
    if ~patient.planReferenceObjects.rtDosesForPlan.isKey(planSopInstanceUid)
        throw(MException('getRtDoseForPlan:DataNotAvailable', ['the requested Rtdose(s) for plan ' planSopInstanceUid ' is not available because reference object is not complete']));
    end
    logger = logging.getLogger('dicom-file-interface');
    
    rtDose = RtDose();
    list = patient.planReferenceObjects.rtDosesForPlan(planSopInstanceUid);
    for listItem = list
        thisRtDose = createModalityObj(patient.getDicomObj(listItem.sopInstanceUid));
        
        if ~strcmpi('plan', thisRtDose.doseSummationType)
            logger.debug('RtDose was skipped because it was not a plan summation type dose');
            continue;
        end
        
        if max(strcmpi({'varian medical systems', 'cms, inc.', 'adac'}, thisRtDose.manufacturer))
            rtDose(end+1) = thisRtDose; %#ok<AGROW> cannot preallocate!
        else
            logger.info(['RtDose was skipped because of unknown manufacturer: ' thisRtDose.manufacturer ' model: ' thisRtDose.manufacturerModelName]);
        end
    end
    
    if length(rtDose) == 1
        throw(MException('getRtDoseForPlan:DataNotAvailable', ['the requested Rtdose(s) for plan ' planSopInstanceUid ' is not available']));
    else
        rtDose(1) = []; %remove the first item in the list so that i can use end+1 instead of a manual counter
    end
end