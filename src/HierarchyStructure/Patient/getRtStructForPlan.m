function rtStruct = getRtStructForPlan(patient, planSopInstanceUid)
%GETRTSTRUCTFORPLAN gets the RtStruct that is linked to the provided RtPlan SopInstanceUid
%
%rtStruct = getRtStructForPlan(patient, planSopInstanceUid)returns an RtStruct object that is linked to the planSopInstanceUid
%   provided.
%
% See also: PATIENT, DICOMOBJ, CREATEPLANPACKAGE
    if ~patient.planReferenceObjects.rtStructsForPlan.isKey(planSopInstanceUid)
        throw(MException('getRtStructForPlan:DataNotAvailable', ['the requested RtStruct for plan ' planSopInstanceUid ' is not available because reference object is not complete']))
    end
    
    if patient.planReferenceObjects.refUids.isKey(patient.planReferenceObjects.rtStructsForPlan(planSopInstanceUid))
        rtStruct = RtStruct(patient.getDicomObj(patient.planReferenceObjects.rtStructsForPlan(planSopInstanceUid)));
    else
        throw(MException('getRtStructForPlan:DataNotAvailable', ['the requested RtStruct for plan ' planSopInstanceUid ' is not available']))
    end
end