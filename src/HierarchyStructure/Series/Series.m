classdef Series
    %SERIES is a collection of DicomObjs with the same SeriesInstanceUid
    %
    %CONSTRUCTORS
    % this = Series(dicomObj) returns a series object for the provided dicomObj
    %
    % See also: DICOMDATABASE, PATIENT, STUDY, DICOMOBJ, PLANREFERENCEOBJECTS, CREATEPLANPACKAGE 
    
    properties (SetAccess = 'private')
        id
        description
        modality
        sopInstanceUids
        nrOfImages
    end
    
    properties (Access = 'private')
        images
        parsed = false
    end
    
    methods
        function this = Series(dicomObj)
            this.images = containers.Map;
            
            if nargin == 0
                return;
            end
            
            this = this.parseDicomObj(dicomObj);
        end
        
        function this = parseDicomObj(this, dicomObj)
        %PARSEDICOMOBJ(dicomObj) parses the DicomObj for this series
            if ~isa(dicomObj, 'DicomObj') || isempty(dicomObj.sopInstanceUid)
                throw(MException('Series:parseDicomObj:InvalidInput', 'please provide a valid DicomObj'));
            end
            
            if this.images.isKey(dicomObj.sopInstanceUid)
                return; 
            end %return, file already parsed
            
            this.images(dicomObj.sopInstanceUid) = dicomObj;

            if this.nrOfImages == 1 %only parse info for first object assuming details are the same for the other objects of the series
                this = this.parseSeriesInfo(dicomObj);
            end
        end
        
        function out = get.nrOfImages(this)
            out = this.images.Count;
        end
        
        function out = getDicomObject(this, sopInstanceUid)
            warning('deprecated function, please use getDicomObj');
            out = this.getDicomObj(sopInstanceUid);
        end
        
        function out = getDicomObj(this, sopInstanceUid)
        %GETDICOMOBJ returns a DicomObj with sopInstanceUid
            if this.images.isKey(sopInstanceUid)
                out = this.images(sopInstanceUid);
            else
                throw(MException('Series:getDicomObj:DataNotAvailable', ['the request DicomObj with sopInstanceUid: ' sopInstanceUid ' is not available in series']))
            end
        end

        function out = getDicomObjectArray(this, sopInstanceUid)
            warning('deprecated function, please use getDicomObjArray');
            out = this.getDicomObjArray(sopInstanceUid);
        end
        
        function out = getDicomObjArray(this)
        %GETDICOMOBJARRAY returns a list of available DicomObj of the series
            keys = this.sopInstanceUids;
            out = DicomObj();
            for i = 1:this.nrOfImages
                out(i) = modalityToDicomObj(this.images(keys{i}));
            end
        end
        
        function out = get.sopInstanceUids(this)
             out = this.images.keys;
        end
    end
    
    methods (Access = 'private')
        function this = parseSeriesInfo(this, dicomObj)
            this.id = dicomObj.seriesInstanceUid;
            this.description = dicomObj.seriesDescription;
            this.modality = dicomObj.modality;
            this.parsed = true;
        end
    end
end