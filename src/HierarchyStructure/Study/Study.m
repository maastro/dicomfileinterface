classdef Study
    %STUDY is a collection of DicomObjs with the same StudyInstanceUid
    %
    %CONSTRUCTORS
    % this = Study(dicomObj) returns a study object for the provided dicomObj
    %
    % See also: DICOMDATABASE, PATIENT, SERIES, DICOMOBJ, PLANREFERENCEOBJECTS, CREATEPLANPACKAGE 
    
    properties
        id
        description
        nrOfSeries
        seriesInstanceUids
    end
    
    properties (Access = 'private')
        series
        parsed = false
    end
    
    methods
        function this = Study(dicomObj)
            this.series = containers.Map;
            if nargin == 0
                return;
            end
            
            this = this.parseDicomObj(dicomObj);
        end
        
        function this = parseDicomObj(this, dicomObj)
            %PARSEDICOMOBJ(dicomObj) parses the DicomObj for this study
            if ~isa(dicomObj, 'DicomObj') || isempty(dicomObj.sopInstanceUid)
                throw(MException('Study:parseDicomObj:InvalidInput', 'please provide a valid DicomObj'));
            end
            
            seriesInstanceUid = dicomObj.seriesInstanceUid;
            if ~this.series.isKey(seriesInstanceUid)               
                this.series(seriesInstanceUid) = Series(dicomObj);
            else
                this.series(seriesInstanceUid) = this.series(seriesInstanceUid).parseDicomObj(dicomObj);
            end

            if ~this.parsed %only parse info for first object
                this = this.parseStudyInfo(dicomObj);
            end
        end

        function out = get.nrOfSeries(this)
            out = this.series.Count;
        end
        
        function out = get.seriesInstanceUids(this)
             out = this.series.keys;
        end
        
        function out = isSeriesAvailable(this, seriesInstanceUid)
        %ISSERIESAVAILABLE returns a boolean of the requested seriesInstanceUid is available in this Study
            out = this.series.isKey(seriesInstanceUid);
        end
        
        function out = getSeriesObject(this, seriesInstanceUid)
        %GETSERIESOBJECT returns a Series with provided seriesInstanceUid
            if this.isSeriesAvailable(seriesInstanceUid)
                out = this.series(seriesInstanceUid);
            else
                throw(MException('Study:getSeriesObject:DataNotAvailable', ['the requested series with seriesInstanceUid ' seriesInstanceUid ' is not available']))
            end
        end
        
        function out = getDicomObjArray(this)
        %GETDICOMOBJECTARRAY returns a list of available DicomObj of the study
            keys = this.seriesInstanceUids;
            if isempty(keys)
                out = DicomObj();
                return;
            end
            
            nrOfObjects = 0;
            seriesObj(1:this.nrOfSeries) = Series;
            for i = 1:this.nrOfSeries
                seriesObj(i) = this.getSeriesObject(keys{i});
                nrOfObjects = nrOfObjects+seriesObj(i).nrOfImages;
            end
            
            out(1:nrOfObjects) = DicomObj();
            start = 1;
            for i = 1:this.nrOfSeries
                out(start:start+seriesObj(i).nrOfImages-1) = seriesObj(i).getDicomObjArray();
                start = start+seriesObj(i).nrOfImages;
            end
        end
    end
    
    methods (Access = private)
        function this = parseStudyInfo(this, dicomObj)
            this.id = dicomObj.studyInstanceUid;
            this.parsed = true;
        end
    end
    
end