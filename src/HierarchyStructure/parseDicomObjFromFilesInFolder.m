function anyDicomObjLister = parseDicomObjFromFilesInFolder(anyDicomObjLister, folder)
%PARSEDICOMOBJFROMFILESINFOLDER is a helper function that prevents repeating code when adding DicomObj to a
% "lister" class (e.g. DicomDatabase or Patient). If the provided lister has a parseDicomObj function this
% helper function will loop over the files found in the folder and add them to the lister instance

if ~ismethod(anyDicomObjLister, 'parseDicomObj')
    throw(MException('addFolderWithDicomObj:InvalidInput', 'provided lister class does not implement a "parseDicomObj(dicomObj) function"'));
end

if ~isdir(folder)
    throw(MException('addFolderWithDicomObj:InvalidInput', 'provided folder does not exist'));
end

logger = logging.getLogger('dicom-file-interface');
anyDicomObjLister = addFileToLister(anyDicomObjLister, rdir(fullfile(folder, '**', '*')), logger);
end

function lister = addFileToLister(lister, fileNames, logger)
    if isempty(fileNames)
        return;
    end
    
    try
        lister = lister.parseDicomObj(DicomObj(fileNames(1).name));
        logger.debug(['added file ' convertPathToUnix(fileNames(1).name) ' to dicomObjLister ' class(lister)]);
    catch exception
        logger.error(['could not parse dicomObj for ' fileNames(1).name]);
        logger.debug(getReport(exception, 'extended', 'hyperlinks', 'off'));
    end
    
    fileNames(1) = [];
    lister = addFileToLister(lister, fileNames, logger);
end

