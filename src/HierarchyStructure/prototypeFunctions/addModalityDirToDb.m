function [ dicomDb ] = addModalityDirToDb(dicomDb, rootDir, modality )
%ADDMODALITYDIRTODB [please add info on me here :<]
    modalityDir = fullfile(rootDir, modality);
    if ~exist(modalityDir, 'dir')
        return;
    end
    
    files = rdir(fullfile(modalityDir, '**', '*'));
    dicomDb = addDicomObjToDatabase(dicomDb, files);
end

