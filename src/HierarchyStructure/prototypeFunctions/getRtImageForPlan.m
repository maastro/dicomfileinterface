function rtImage = getRtImageForPlan(patient, planUid)
%GETRTIMAGEFORPLAN  is part of a library of functions to find DICOM references.
%
% ctScan = getRtImageForPlan(patient, planUid) returns an RtImage object that is linked to the planSopInstanceUid
%   provided.
%
% See also: PATIENT, DICOMOBJ, CREATEPLANPACKAGE
    warning('this function is not implemented correctly, needs to be updated!')
    if ~patient.planReferenceObjects.rtimageForPlan.isKey(planUid)
        rtImage = [];
        return;
    end
    list = patient.planReferenceObjects.rtimageForPlan(planUid);
    rtImage = RtImage();
    for i = 1:length(list)
        rtImage(i) = createModalityObj(patient.getDicomObject(list(i).sopInstanceUid));
    end
end