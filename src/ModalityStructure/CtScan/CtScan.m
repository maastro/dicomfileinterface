classdef CtScan
    %CTSCAN representation of an entire DICOM CT-SCAN
    %
    %CONSTRUCTORS
    % this = CtScan(folder) creates this object using
    %   a folder with CT dicom files
    %
    % This CT is given in the image coordinate system
    %               -----------         IEC
    %              /|         /|         Z
    %             / |        / |        /|\
    %            /  |       /  |         |   /|\ Y
    %           /   |      /   |         |   /
    %          /    ------/----          |  /
    %          ----/------    /          | /
    %         |   /      |   /           |/
    %         |  /       |  /     X------|----------------->
    %         | /        | /            /|
    %         |/         |/            / |
    %         ------------
    %
    % Origin coordinates will be the bottom-left-corner. The CT cube will
    % be addressed in the following way pixelData(1:columns,1:numberOfSlices,1:rows)
    %
    % See also: DICOMOBJ, CTSLICE, CREATEIMAGEFROMCT
    properties
        ctSlices = CtSlice()
        seriesInstanceUid
        studyInstanceUid
        instanceSortedCtSlices
        ySortedCtSlices
        numberOfSlices
        sliceThickness
        hasUniformThickness
        pixelSpacingX
        pixelSpacingY
        pixelSpacingZ
        originX
        originY
        originZ
        realX
        realY
        realZ
        pixelData
    end
    
    properties (Access = private)
        logger;
    end
    
    methods
        function this = CtScan(folder)
            this.logger = logging.getLogger('dicom-file-interface');
            
            if nargin == 0 %preserve standard empty constructor
                return;
            end
            
            if ~ischar(folder) || ~isdir(folder)
                throw(MException('CtScan:constructor:InvalidInput', 'invalid input type, please give a folder location'));
            end
            
            this.logger.info(['reading all files in folder ' convertPathToUnix(folder)])
            files = rdir(fullfile(folder, '**', '*'));
            this = this.addListOfFiles(files);
        end
        
        function this = addListOfObjects(this, dicomObj)
            % ADDLISTOFOBJECTS(dicomObj) loops over an array of DicomObjs to construct
            %  the CtSlices that provide info of the CtScan
            if ~ isa(dicomObj, 'DicomObj')
                throw(MException('CtScan:addListOfObjects:InvalidInput', 'please provide an array of DicomObjs'));
            end
            
            for i = 1:length(dicomObj)
                if isa(dicomObj(i), 'CtSlice')
                    this = this.addCtSlices(ctSlice);
                else                    
                    this = this.addCtSlices(CtSlice(dicomObj(i)));            
                end
            end
        end
        
        function this = addListOfFiles(this, files)
            %ADDLISTOFFILES(files) loops over a rdir array of fullfile locations
            % to construct a CtScan by reading each individual slice.
            dicomObj(1:length(files)) = DicomObj;
            for i = 1:length(files)
                dicomObj(i) = DicomObj(files(i).name);
            end
            
            this = this.addListOfObjects(dicomObj);
        end
        
        function this = addCellArrayOfFiles(this, files)
            %ADDCELLARRAYOFFILES(files) loops over a cell array of fullfile locations
            % to construct a CtScan by reading each individual slice.
            dicomObj(1:length(files)) = DicomObj;
            for i = 1:length(files)
                dicomObj(i) = DicomObj(files{i});
            end
            
            this = this.addListOfObjects(dicomObj);
        end
        
        function this = readDicomData(this)
            % READDICOMDATA() loops over all CtSlices in the CtScan and read the
            %  binary image data of the DicomFiles and converts them to the previously
            %  mentioned coordinate system
            if ~isempty(this.pixelData)
                warning('pixel data already loaded, skipping ctScan.readDicomData');
                return;
            end
            
            for i = 1:this.numberOfSlices
                this.ctSlices(i) = this.ctSlices(i).readDicomData();
            end
            this.pixelData = this.createIecImage();
        end
        
        % -------- START GETTERS/SETTERS ----------------------------------
        function out = get.ctSlices(this)
            if ~isempty(this.ctSlices(1).dicomHeader)
                out = this.ctSlices;
            else
                out = CtSlice();
            end
        end
        
        function out = get.instanceSortedCtSlices(this)
            if ~isempty(this.ctSlices(1).dicomHeader)
                out = orderStructureArray(this.ctSlices, 'instanceNumber');
            else
                out = this.ctSlices;
            end
        end
        
        function out = get.ySortedCtSlices(this)
            if ~isempty(this.ctSlices(1).dicomHeader)
                out = orderStructureArray(this.ctSlices, 'y');
            else
                out = this.ctSlices;
            end
        end
        
        function out = get.numberOfSlices(this)
            out = length(this.ctSlices);
        end
        
        function out = get.hasUniformThickness(this)
            out = false;
            if length(this.sliceThickness) == 1
                out = true;
            end
        end
        
        function out = get.sliceThickness(this)
            slices = this.ySortedCtSlices;
            out = zeros(1, this.numberOfSlices);
            for i = 1:this.numberOfSlices
                out(i) = slices(i).sliceThickness;
            end
            out = unique(out);
        end
        
        function out = get.pixelSpacingY(this)
            out = this.sliceThickness;
        end
        
        function out = get.pixelSpacingX(this)
            slices = this.ySortedCtSlices;
            out = zeros(this.numberOfSlices,1);
            for i = 1:this.numberOfSlices
                out(i) = slices(i).pixelSpacing(1);
            end
            out = unique(out);
        end
        
        function out = get.pixelSpacingZ(this)
            slices = this.ySortedCtSlices;
            out = zeros(this.numberOfSlices,1);
            for i = 1:this.numberOfSlices
                out(i) = slices(i).pixelSpacing(2);
            end
            out = unique(out);
        end
        
        function out = get.originX(this)
            if this.ySortedCtSlices(1).imageOrientationPatient(1) == 1
                out = this.ySortedCtSlices(1).x;
            elseif this.ySortedCtSlices(1).imageOrientationPatient(1) == -1
                out = this.ySortedCtSlices(1).x - ...
                    (this.pixelSpacingX * this.ctSlices(1).columns);
            else
                out = [];
                warning('unsupported ImageOrientationPatient detected, cannot provide origin');
            end
        end
        
        function out = get.originY(this)
            out = this.ySortedCtSlices(1).y;
        end
        
        function out = get.originZ(this)
            if this.ySortedCtSlices(1).imageOrientationPatient(5) == -1
                out = -this.ySortedCtSlices(1).z;
            elseif this.ySortedCtSlices(1).imageOrientationPatient(5) == 1
                out = -this.ySortedCtSlices(1).z - ...
                    (this.pixelSpacingZ * (this.ySortedCtSlices(1).rows - 1));
            else
                out = [];
                warning('unsupported ImageOrientationPatient detected, cannot provide origin');
            end
        end
        
        function out = get.realX(this)
            out = (this.originX : this.pixelSpacingX : (this.originX + (this.ctSlices(1).columns - 1) * this.pixelSpacingX))';
        end
        
        function out = get.realY(this)
            out = (this.originY : this.pixelSpacingY : (this.originY + (this.numberOfSlices - 1) * this.pixelSpacingY))';
        end
        
        function out = get.realZ(this)
            out = (this.originZ : this.pixelSpacingZ : (this.originZ + (this.ctSlices(1).rows - 1) * this.pixelSpacingZ))';
        end
        
        function out = get.seriesInstanceUid(this)
            out = this.ySortedCtSlices(1).seriesInstanceUid;
        end
        
        function out = get.studyInstanceUid(this)
            out = this.ySortedCtSlices(1).studyInstanceUid;
        end
    end
    
    methods (Access = 'private')
        function this = addCtSlices(this, ctSlice)
            if isempty(this.ctSlices(1).dicomHeader)
                this.ctSlices(1) = ctSlice;
            else
                index = length(this.ctSlices) + 1;
                this.ctSlices(index) = ctSlice;
            end
        end
        
        function image = createIecImage(this)
            slices = this.ySortedCtSlices;
            image = zeros(slices(1).columns, slices(1).rows, this.numberOfSlices);
            %ASSUMPTION MADE -> rows and colums do not change within one scan
            for i = 1:this.numberOfSlices
                image(:,:,i) = slices(i).scaledImageData;
            end
            %convert image to IEC format (see top comment for more info)
            image(:,:,:) = image(end:-1:1,:,:);
            image = permute(image,[2 3 1]);
        end
    end
end