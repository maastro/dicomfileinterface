function image = createImageFromCt(ctScan)
% CREATEIMAGEFROMCT creates an Image object using data from the CtScan object
%
% image = CREATEIMAGEFROMCT(ctScan) returns an image object which represents the data
%  in the CtScan dicom object.
%
% See also: CTSCAN, CTSLICE, CREATEIMAGEFROMCTPROPERTIES
warning('deprecated, will remove in next version, please use createImageFromCtScan');
image = createImageFromCtScan(ctScan);