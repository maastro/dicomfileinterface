function image = createImageFromCtScan(ctScan)
% CREATEIMAGEFROMCTSCAN creates an Image object using data from the CtScan object
%
% image = CREATEIMAGEFROMCTSCAN(ctScan) returns an image object which represents the data
%  in the CtScan dicom object.
%
% See also: CTSCAN, CTSLICE, CREATEIMAGEFROMCTPROPERTIES
if ~ctScan.hasUniformThickness
    throw(MException('createImageFromCt:InvalidInput', 'CT scans with changing sliceThickness are not supported'));
end

if ~isequal(ctScan.ySortedCtSlices(1).imageOrientationPatient,...
        [1;0;0;0;1;0]);
    throw(MException('createImageFromCt:ProcessingError', 'Unsupported ImagePostionPatient for provided CT scan'));
end

image = Image(ctScan.pixelSpacingX, ctScan.pixelSpacingY, ctScan.pixelSpacingZ, ctScan.realX, ctScan.realY, ctScan.realZ, ctScan.pixelData);
end