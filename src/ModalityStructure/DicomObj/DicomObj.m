classdef DicomObj
    %DICOMOBJ basic DICOM object that can be extended for other modalities
    %
    %CONSTRUCTORS
    % this = DicomObj(fileStr) creates DicomObj to represent the data of the dicom
    %  header of a file. useVrHeuristics determines if the matlab datadictionary or the filemeta
    %  data is leading in translating the header.
    %
    % See also: DICOMINFO, DICOMREAD, DICOMINFOWRAPPER
    
    properties
        dicomHeader
        pixelData
        
        patientId
        studyInstanceUid
        seriesInstanceUid
        seriesDescription
        sopInstanceUid
        frameOfReferenceUid
        modality
        
        filename
        x %in cm
        y %in cm
        z %in cm
        rows %same as heigth, Z
        columns %same as width, X
        pixelSpacing
        imageOrientationPatient
        imagePositionPatient
        instanceNumber
        
        firstname
        lastname
        gender
        dateOfBirth
        
        manufacturer
        manufacturerModelName
        
        logger;
    end
    
    properties (Access = private)
        IEC_MM_TO_CM = 10;
    end
    
    methods
        function this = DicomObj(filename)
            this.logger = logging.getLogger('dicom-file-interface');
            
            if nargin == 0 %preserve standard empty constructor
                return;
            end
            
            this = this.readDicomHeader(filename);
        end
        
        function this = readDicomHeader(this, filename)
            %READDICOMHEADER(fileName) read the dicomheader of the file
            % at the provided file location
            if ~isempty(this.dicomHeader);
                return;
            end
            
            if ~ischar(filename)
                throw(MException('DicomObj:readDicomHeader:InvalidInput', ['Provided input should be a string! provided was:' class(filename)]));
            end
            
            if ~exist(filename, 'file')
                throw(MException('DicomObj:readDicomHeader:DataNotAvailable', ['DICOM file ''' convertPathToUnix(filename) ''' not found.''']));
            end
            
            if ~isdicom(filename)
                throw(MException('DicomObj:readDicomHeader:DataNotAvailable', ['File ''' convertPathToUnix(filename) ''' is not a valid dicom file''']));
            end
            
            this.logger.debug(['Create DicomObj reading ' convertPathToUnix(filename)]);
            this.dicomHeader = dicominfoWrapper(filename);
        end
        
        function this = readDicomData(this)
            %READDICOMDATA read the dicom pixel data of the DICOM file
            if ~isempty(this.pixelData)
                return;
            end
            
            this = this.readDicomDataOverwrite();
        end
        
        function this = readDicomDataOverwrite(this)
            %READDICOMDATAOVERWRITE ignores the safety that the data will not be re-read and stores 
            % the image block of the dicom file in pixelData
            if ~exist(this.filename, 'file')
                throw(MException('DicomObj:readDicomDataOverwrite:ProcessingError', ...
                    ['The file ' convertPathToUnix(this.filename) ' in the dicom header has become unavailable for reading!']));
            end
            
            this.pixelData = dicomread(this.filename);
        end
        
        function [status, this] = writeToFile(this, path)
            % WRITETOFILE(path) writes the dicomObj represented to a new file at the provided location
            if ~exist(path, 'dir')
                throw(MException('DicomObj:writeToFile:InvalidInput', ...
                    ['DICOM file ''' convertPathToUnix(path) ''' not found.''']));
            end
            
            this.dicomHeader.Filename = fullfile(path, [upper(this.modality) '_' this.sopInstanceUid '.dcm']);
            status = dicomwrite(this.pixelData, ...
                this.filename, ...
                this.dicomHeader, 'CreateMode', 'copy');
        end
        
        function subClassObj = copyObj(this, subClassObj)
            % COPYOBJ copies the data of this object to the provided subClassObj and returns the subClassObj
            subClassObj.dicomHeader = this.dicomHeader;
            subClassObj.pixelData = this.pixelData;
        end
        
        % -------- START GETTERS/SETTERS ----------------------------------
        function this = set.dicomHeader(this, header)
            if isfield(header, 'Format') && strcmpi(header.Format, 'dicom')
                this.dicomHeader = header;
            else
                throw(MException('DicomObj:set:dicomHeader:InvalidInput','the provided input is an invalid dicom header'));
            end
        end
        
        function out = get.patientId(this)
            out = [];
            if ~isempty(this.dicomHeader)
                out = this.dicomHeader.PatientID;
            end
        end
        
        function out = get.studyInstanceUid(this)
            out = [];
            if ~isempty(this.dicomHeader)
                out = this.dicomHeader.StudyInstanceUID;
            end
        end
        
        function out = get.seriesInstanceUid(this)
            out = [];
            if ~isempty(this.dicomHeader)
                out = this.dicomHeader.SeriesInstanceUID;
            end
        end
        
        function out = get.seriesDescription(this)
            out = [];
            if isfield(this.dicomHeader, 'SeriesDescription')
                out = this.dicomHeader.SeriesDescription;
            end
        end
        
        function out = get.sopInstanceUid(this)
            out = [];
            if ~isempty(this.dicomHeader)
                out = this.dicomHeader.SOPInstanceUID;
            end
        end
        
        function out = get.frameOfReferenceUid(this)
            out = [];
            if isfield(this.dicomHeader, 'FrameOfReferenceUID')
                out = this.dicomHeader.FrameOfReferenceUID;
            elseif isfield(this.dicomHeader, 'ReferencedFrameOfReferenceSequence')
                out = this.dicomHeader.ReferencedFrameOfReferenceSequence.Item_1.FrameOfReferenceUID;
            end
        end
        
        function out = get.filename(this)
            out = [];
            if ~isempty(this.dicomHeader)
                out = convertPathToUnix(this.dicomHeader.Filename);
            end
        end
        
        function out = get.modality(this)
            out = [];
            if ~isempty(this.dicomHeader)
                out = lower(this.dicomHeader.Modality);
            end
        end
        
        function out = get.rows(this)
            out = [];
            if isfield(this.dicomHeader, 'Rows')
                out = double(this.dicomHeader.Rows);
            end
        end
        
        function out = get.columns(this)
            out = [];
            if isfield(this.dicomHeader, 'Columns')
                out = double(this.dicomHeader.Columns);
            end
        end
        
        function out = get.pixelSpacing(this)
            out = [];
            if isfield(this.dicomHeader, 'PixelSpacing')
                out = this.dicomHeader.PixelSpacing/this.IEC_MM_TO_CM;
            end
        end
        
        function out = get.imageOrientationPatient(this)
            out = [];
            if isfield(this.dicomHeader, 'ImageOrientationPatient')
                out = this.dicomHeader.ImageOrientationPatient;
            end
        end
        
        function out = get.imagePositionPatient(this)
            out = [];
            if isfield(this.dicomHeader, 'ImagePositionPatient')
                out = this.dicomHeader.ImagePositionPatient;
            end
        end
        
        function out = get.x(this)
            out = [];
            if isfield(this.dicomHeader, 'ImagePositionPatient')
                out = this.dicomHeader.ImagePositionPatient(1)/this.IEC_MM_TO_CM;
            end
        end
        
        function out = get.y(this)
            out = [];
            if isfield(this.dicomHeader, 'ImagePositionPatient')
                out = this.dicomHeader.ImagePositionPatient(3)/this.IEC_MM_TO_CM;
            end
        end
        
        function out = get.z(this)
            out = [];
            if isfield(this.dicomHeader, 'ImagePositionPatient')
                out = this.dicomHeader.ImagePositionPatient(2)/this.IEC_MM_TO_CM;
            end
        end
        
        function out = get.lastname(this)
            out = [];
            if isfield(this.dicomHeader, 'PatientName') && isfield(this.dicomHeader.PatientName, 'FamilyName')
                out = this.dicomHeader.PatientName.FamilyName;
            end
        end
        
        function out = get.firstname(this)
            out = [];
            if isfield(this.dicomHeader, 'PatientName') && isfield(this.dicomHeader.PatientName, 'FirstName')
                out = this.dicomHeader.PatientName.FirstName;
            end
        end
        
        function out = get.gender(this)
            out = [];
            if isfield(this.dicomHeader, 'PatientSex')
                out = this.dicomHeader.PatientSex;
            end
        end
        
        function out = get.dateOfBirth(this)
            out = [];
            if isfield(this.dicomHeader, 'PatientBirthDate')
                out = this.dicomHeader.PatientBirthDate;
            end
        end
        
        function out = get.instanceNumber(this)
            out = [];
            if isfield(this.dicomHeader, 'InstanceNumber')
                out = this.dicomHeader.InstanceNumber;
            end
        end
        
        function out = get.manufacturer(this)
            out = [];
            if isfield(this.dicomHeader, 'Manufacturer')
                out = lower(this.dicomHeader.Manufacturer);
            end
        end
        
        function out = get.manufacturerModelName(this)
            out = [];
            if isfield(this.dicomHeader, 'ManufacturerModelName')
                out = lower(this.dicomHeader.ManufacturerModelName);
            end
        end
    end
end