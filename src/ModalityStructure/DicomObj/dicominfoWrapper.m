function header = dicominfoWrapper(file, varargin)
% DICOMINFOWRAPPER is a wrapper for Matlab's dicominfo 
%	DICOMINFOWRAPPER works around the fact that Matlab is throwing
%	warnings instead of interceptable exceptions for non-compliant DICOM
%	files 
% 
%   It's syntax is compliant to the syntax of DICOMINFO
%
%   See also DICOMINFO

    try
        trapWarningState();
        
        % Restore warning state at exit or when halted
        c = onCleanup(@() restoreWarningState);

        header = dicominfo(file, varargin{:});
    catch exception
        params = determineSettingsFromWarning(exception, varargin);
        header = dicominfoWrapper(file, params{:});
    end
end

function params = determineSettingsFromWarning(exception, varargin)
    switch exception.identifier
        case 'images:dicomparse:vrHeuristic'
            params = [varargin{:}, {'UseVRHeuristic', false}];
        case 'images:dicominfo:fileVRDoesNotMatchDictionary'
            params = [varargin{:}, {'UseDictionaryVR', true}];
        otherwise
            restoreWarningState();
            rethrow(exception);
    end
end

function trapWarningState
    % Trap some specific warnings thrown by matlab, which should have been
    % exceptions so we can properly handle them. 
    % See http://undocumentedmatlab.com/blog/trapping-warnings-efficiently
    
    % Intercept exceptions for files to make datatypes in INFO conform to
    % the data dictionary, regardless of what information is present in the
    % file.
    warning('error', 'images:dicominfo:fileVRDoesNotMatchDictionary'); %#ok<CTPCT> the matlab documentation says this MLINT warning is incorrect

    % Intercept exceptions for files that are noncompliant and switch
    % value representation (VR) modes incorrectly
    warning('error', 'images:dicomparse:vrHeuristic'); %#ok<CTPCT>

    % Not sure if below error has a parameter to suppres it or that it
    % is a restult of incorreclty interpretation of explicit VR, for
    % which the above error shoudl do.
    warning('error', 'images:dicomparse:shortImport'); %#ok<CTPCT>
end

function restoreWarningState
    warning('on', 'images:dicomparse:vrHeuristic');
    warning('on', 'images:dicominfo:fileVRDoesNotMatchDictionary');
    warning('on', 'images:dicomparse:shortImport');
end