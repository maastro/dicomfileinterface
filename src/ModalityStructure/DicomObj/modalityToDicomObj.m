function dicomObj = modalityToDicomObj(modalityObj)
%MODALITYTODICOMOBJ converts a modality object to a genaric dicomObj
%
% dicomObj = modalityToDicomObj(modalityObj) will convert the Modality Specific object to a general
%  DicomObj which is helpful when wanting to store all objects into one array, they can be converted
%  into a modality object again when they are needed. This makes the object management easier.
%
% See also: DICOMOBJ, RTDOSE, RTIMAGE, RTPLAN, RTSTRUCT, CREATEMODALITYOBJ
    if isa(modalityObj, 'DicomObj')
        dicomObj = modalityObj.copyObj(DicomObj());
    else
        throw(MException('modalityToDicomObj:InvalidInput', 'the provided object has to be a subclass of DicomObj'))
    end
end

