classdef Image
    %IMAGE contains sampled information of a real world space. Representing the binary data of DicomObj 
    %
    %CONSTRUCTOR
    % this = Image(pixelSpacingX, pixelSpacingY, pixelSpacingZ, realX, realY, realZ, pixelData)
    %  use the "createImage..." functions to properly create an image object 
    %
    % See also: MATCHIMAGEREPRESENTATION, VOLUMEOFINTEREST, CALCULATEIMAGESTATISTICS,
    % PLOTSLICEOFIMAGE, CREATEIMAGEFROMRTDOSE, CREATEIMAGEFROMCT
    properties 
        pixelSpacingX %in cm
        pixelSpacingY %in cm
        pixelSpacingZ %in cm
        realX %in cm
        realY %in cm
        realZ %in cm
    end
    
    properties (SetAccess = 'protected')
        rows
        slices
        columns
        pixelData
        is3d
    end
    
    methods        
        function this = Image(pixelSpacingX, pixelSpacingY, pixelSpacingZ, realX, realY, realZ, pixelData)
            if nargin == 0 %preserve standard empty constructor
                return;
            end
            
            this = this.addImageDimensions(pixelSpacingX, pixelSpacingY, pixelSpacingZ, realX, realY, realZ);
            
            if ~isempty(pixelData)
                this = this.addPixelData(pixelData);
            end
        end
        
        function this = addImageDimensions(this, pixelSpacingX, pixelSpacingY, pixelSpacingZ, realX, realY, realZ)
        %ADDIMAGEDIMENSIOONS(pixelSpacingX, pixelSpacingY, pixelSpacingZ, realX, realY, realZ) ...
            this.pixelSpacingX = pixelSpacingX;
            this.pixelSpacingY = pixelSpacingY;
            this.pixelSpacingZ = pixelSpacingZ;
            this.realX = realX;
            this.realY = realY;
            this.realZ = realZ;
        end
        
        function this = copyImageProperties(this, image)
            %COPYIMAGEPROPERTOES(image) is an alternative to creating a VolumeOfInterest, create an empty
            %   VolumeOfInterest and parse an existing image.
            this = this.addImageDimensions(image.pixelSpacingX, ...
                image.pixelSpacingY, ...
                image.pixelSpacingZ, ...
                image.realX, ...
                image.realY, ...
                image.realZ);
        end
        
        function this = addPixelData(this, pixelData)
        %ADDPIXELDATA(pixelData) adds a matrix of pixel data to the value class
        % after checking if the dimensions match.
            if size(pixelData,1) ~= this.columns || ...
               size(pixelData,2) ~= this.slices || ...
               size(pixelData,3) ~= this.rows
                throw(MException('Image:addPixelData:InvalidInput', ['Dimension mismatch! The real axis properties' ...
                                  ' do not match the dimensions of the image you are trying to store']));
            end
            
            this.pixelData = pixelData;
        end
        
        % -------- START GETTERS/SETTERS ----------------------------------
        function out = get.rows(this)
            out = length(this.realZ);
        end
        
        function out = get.columns(this)
            out = length(this.realX);
        end
        
        function out = get.slices(this)
            out = length(this.realY);
        end
        
        function out = get.is3d(this)
            out = false; 
            if this.rows > 1 && this.columns > 1 && this.slices > 1
                out = true;
            end
        end
        
        function this = set.pixelSpacingX(this, pixelSpacing)
            if ~isnumeric(pixelSpacing)
                throw(MException('Image:set_pixelSpacingX:InvalidInput', 'pixelSpacingX has to be a numeric value'));
            end
            this.pixelSpacingX = pixelSpacing;
        end
        
        function this = set.pixelSpacingY(this, pixelSpacing)
            if ~isnumeric(pixelSpacing)
                throw(MException('Image:set_pixelSpacingY:InvalidInput', 'pixelSpacingY has to be a numeric value'));
            end
            this.pixelSpacingY = pixelSpacing;
        end
        
        function this = set.pixelSpacingZ(this, pixelSpacing)
            if ~isnumeric(pixelSpacing)
                throw(MException('Image:set_pixelSpacingZ:InvalidInput', 'pixelSpacingZ has to be a numeric value'));
            end
            this.pixelSpacingZ = pixelSpacing;
        end
        
        function this = set.realX(this, real)
            if ~isnumeric(real)
                throw(MException('Image:set_realX:InvalidInput', 'realX has to be a numeric value'));
            end
            this.realX = real;
        end
        
        function this = set.realY(this, real)
            if ~isnumeric(real)
                throw(MException('Image:set_realY:InvalidInput', 'realY has to be a numeric value'));
            end    
            this.realY = real;
        end
        
        function this = set.realZ(this, real)
            if ~isnumeric(real)
                throw(MException('Image:set_realZ:InvalidInput', 'realZ has to be a numeric value'));
            end
            this.realZ = real;
        end
    end
end