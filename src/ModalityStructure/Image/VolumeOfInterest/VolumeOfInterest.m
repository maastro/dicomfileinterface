classdef VolumeOfInterest < Image
    %VOLUMEOFINTEREST is a specialized kind of image to represent a bitmask
    %
    % CONSTRUCTORS
    %  this = VolumeOfInterest(pixelSpacingX, pixelSpacingY, pixelSpacingZ, realX, realY, realZ, pixelData)
    %    the same constructor as the ImageObject
    %
    % See also: IMAGE, CREATEIMAGEDATAFORVOI, CREATEIMAGEDATAFORVOIFULLGRID, CREATECONTOURMASKFROMVOI,
    % ADDCONTOURTOFIGUREFROMVOI, ADDCONTOURFILLTOFIGUREFROMVOI
    
    properties
        volume
        
        xCompressed = []
        yCompressed = []
        zCompressed = []
        
        uncompressedPixelData
        EDGE_BUFFER = 0;        
    end
    
    methods
        function this = VolumeOfInterest(pixelSpacingX, pixelSpacingY, pixelSpacingZ, realX, realY, realZ, pixelData)
            if nargin == 0 %preserve standard empty constructor
                return;
            end
            
            this = this.addImageDimensions(pixelSpacingX, pixelSpacingY, pixelSpacingZ, realX, realY, realZ);
            
            if ~isempty(pixelData)
                this = this.addPixelData(pixelData);
            end
        end

        function this = addPixelData(this, pixelData)
            %ADDPIXELDATA(pixelData) is a overwritten function of Image()
            % which adds compressions functionality and convert the image to a logical matrix.
            this = this.addPixelData@Image(pixelData);
            this.pixelData = logical(pixelData);
            this = this.compress();
        end
        
        function a = plus(a, b)
            if ~canBeCombined(a,b)
                throw(MException('VolumeOfInterest:plus:InvalidInput', 'cannot add VolumesOfInterest, dimensions do not agree'));
            end
            newPixelData = a.uncompressedPixelData | b.uncompressedPixelData;
            a = a.addPixelData(newPixelData);
        end
        
        function a = minus(a, b)
            if ~canBeCombined(a,b)
                throw(MException('VolumeOfInterest:minus:InvalidInput', 'cannot add VolumesOfInterest, dimensions do not agree'));
            end
            newPixelData = a.uncompressedPixelData &~ b.uncompressedPixelData;
            a = a.addPixelData(newPixelData);
        end
        
        % -------- START GETTERS/SETTERS ----------------------------------
        function out = get.uncompressedPixelData(this)
            out = false(this.columns, this.slices, this.rows);
            out(this.xCompressed, this.yCompressed, this.zCompressed) = this.pixelData;
        end
        
        function out = get.volume(this)
            out = sum(double(this.pixelData(:))).*(this.pixelSpacingX*this.pixelSpacingY*this.pixelSpacingZ);
        end
    end
    
    methods (Access = protected)
        function out = canBeCombined(a, b)
            out = true;
            if ~isequal(a.realX, b.realX) || ...
                    ~isequal(a.realY, b.realY) || ...
                    ~isequal(a.realZ, b.realZ)
                out = false;
            end
        end
    end
    
    methods (Access = private)
        function this = compress(this)
            [x,y,z] = findVolumeEdges(this);
            if ~isempty(x)
                this.xCompressed = x;
                this.yCompressed = y;
                this.zCompressed = z;
                this.pixelData = this.pixelData(x,y,z);
            else
                this.xCompressed = 1:this.columns;
                this.yCompressed = 1:this.slices;
                this.zCompressed = 1:this.rows;
            end
        end
        
        function [x,y,z] = findVolumeEdges(this)
            [x,y,z]=ind2sub(size(this.pixelData),find(this.pixelData));
            x = sort(unique(x));
            y = sort(unique(y));
            z = sort(unique(z));
            
            if ~isempty(x)
                x = this.determineIndexArray(x, this.columns);
                y = this.determineIndexArray(y, this.slices);
                z = this.determineIndexArray(z, this.rows);
            end
        end
        
        function x = determineIndexArray(this, x, upperLimit)
            if (x(1)-this.EDGE_BUFFER) < 1
                first = 1;
            else
                first = x(1)-this.EDGE_BUFFER;
            end
            if (x(end)+this.EDGE_BUFFER) > upperLimit
                last = upperLimit;
            else
                last = x(end)+this.EDGE_BUFFER;
            end
            x = first:last;
        end
    end
end