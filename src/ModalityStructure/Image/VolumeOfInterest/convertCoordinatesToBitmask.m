function binaryImage = convertCoordinatesToBitmask(contours, xAxis, yAxis, zAxis, precision)
%CONVERTBINARYCOORDINATEDTOBITMASK creates a bitmask for closed contours.
%
%binaryImage = convertCoordinatesToBitmask(contours, xAxis, yAxis, zAxis) creates a
% binaryImage which represents the inside of the contours (double(any,3) [x,y,z]) on the real axis
% provided (xAxis, yAxis, zAxis)
%
%binaryImage = convertCoordinatesToBitmask(contours, xAxis, yAxis, zAxis, precision) lets you
% influence the precision with wich the doubles are converted to int16 to be able to compare them.
%
% see also: INTERP1, POLY2MASK
    if nargin == 4
        precision = 2;
    end
    logger = logging.getLogger('dicom-file-interface');
    logger.debug('converting contour coordinates to bitmask');
    
    binaryContours = convertContoursToBinarySlices(contours, xAxis, yAxis);
    zAxisContours = unique(cellfun(@(xyz)xyz(1,3),contours));
    zAxis = int16(round(zAxis.*10^precision));
    zAxisContours = int16(round(zAxisContours.*10^precision));
    
    iContourOnAxis = findIndexOfContoursOnAxis(zAxisContours, zAxis);
    iContourBetweenAxis = findIndexOfContoursBetweenAxis(zAxisContours, zAxis);
    dContourBetweenAxis = findDistanceOfContourToAxis(zAxisContours, zAxis, iContourBetweenAxis);
    
    logger.debug([ num2str(length(binaryContours)) ' binary contour slices made. ' ...
                   num2str(length(iContourBetweenAxis)) ' slices were available on the same axis as the reference axis.'...
                   num2str(length(iContourBetweenAxis)) ' slices need to be interpreted to fit on axis.']);
    
    wantedOutputIndex = false(length(zAxis),1);    
    iFirst = findIndexFirstContourSliceOnAxis(iContourOnAxis, iContourBetweenAxis);
    iLast = findIndexLastContourSliceOnAxis(iContourOnAxis, iContourBetweenAxis);
    wantedOutputIndex(iFirst:iLast) = true;    
    
    binaryImage = false(size(binaryContours{1},1),size(binaryContours{1},2),length(iContourOnAxis));
    binaryImage = storeContoursOnAxis(binaryContours, binaryImage, wantedOutputIndex, iContourOnAxis);
    
    if sum(wantedOutputIndex) ~= sum(~isnan(iContourOnAxis)); %not all contours are one axis.
        binaryImage = determineBitmaskBetweenAxis(binaryContours, binaryImage, wantedOutputIndex, iContourOnAxis, iContourBetweenAxis, dContourBetweenAxis);
        binaryImage = determineBitmaskAtTheBeginning(binaryContours{1},  binaryImage, wantedOutputIndex, iContourBetweenAxis);
        binaryImage = determineBitmaskAtTheEnd(binaryContours{end},  binaryImage, wantedOutputIndex, iContourBetweenAxis);
    end
end

function binaryContours = convertContoursToBinarySlices(contours, xAxis, yAxis)
    [~,~,iUniqueContours] = unique(cellfun(@(xyz)xyz(1,3),contours));
        %[C,ia,ic] = unique(A)
        %If A is a vector, then C = A(ia) and A = C(ic).
    previousUniqueContour = 0;
    binaryContours = cell(iUniqueContours(end),1);
    for i = 1 : length(contours)
        binaryContour = poly2mask( ...
            interp1(xAxis, 1:length(xAxis), contours{i}(:,1),'linear','extrap'), ...
            interp1(yAxis, 1:length(yAxis), contours{i}(:,2),'linear','extrap'), ...
            length(yAxis), length(xAxis));

        if iUniqueContours(i) ~= previousUniqueContour;
            binaryContours{iUniqueContours(i)} = binaryContour;
        else
            binaryContours{iUniqueContours(i)} = binaryContours{iUniqueContours(i)} | binaryContour;
        end

        previousUniqueContour = iUniqueContours(i);
    end
end

function iFirstSlice = findIndexFirstContourSliceOnAxis(contourIndexOnAxis, contourIndexBetweenAxis)
    iFirstSlice = length(contourIndexOnAxis);
    firstContourOnAxis = find(contourIndexOnAxis > 0, 1, 'first');
    if ~isempty(firstContourOnAxis)
        iFirstSlice = firstContourOnAxis;
    end
    firstContourBetweenAxis = find(contourIndexBetweenAxis(:,1) > 0, 1, 'first');
    if firstContourBetweenAxis < iFirstSlice
        iFirstSlice = firstContourBetweenAxis;
    end
end

function iLastSlice = findIndexLastContourSliceOnAxis(contourIndexOnAxis, contourIndexBetweenAxis)
    iLastSlice = 1;
    lastContourOnAxis = find(contourIndexOnAxis > 0, 1, 'last');
    if ~isempty(lastContourOnAxis)
        iLastSlice = lastContourOnAxis;
    end
    lastContourBetweenAxis = find(contourIndexBetweenAxis(:,1) > 0, 1, 'last');
    if lastContourBetweenAxis > iLastSlice
        iLastSlice = lastContourBetweenAxis;
    end
end

function iContourOnAxis = findIndexOfContoursOnAxis(zAxisContours, zAxis)
    iContourOnAxis = NaN(length(zAxis),1);
    for i = 1:length(zAxis)
        contourIsOnAxis = (zAxisContours == zAxis(i));
        if ~isempty(find(contourIsOnAxis, 1))
            iContourOnAxis(i) = find(contourIsOnAxis);
        end
    end
end

function iContourBetween = findIndexOfContoursBetweenAxis(zAxisContours, zAxis)
    iContourBetween = NaN(length(zAxis),2);
    for i = 1:length(zAxis)
        before = find(zAxisContours < zAxis(i), 1, 'last');
        after  = find(zAxisContours > zAxis(i), 1, 'first');
        if ~isempty(before) && ~isempty(after)
            iContourBetween(i,1) = before;
            iContourBetween(i,2) = after;
        end
    end
end

function distance = findDistanceOfContourToAxis(zAxisContours, zAxis, iContourBetween)
    distance = NaN(length(zAxis),2);
    for j = 1:length(zAxis)
        if ~isnan(iContourBetween(j,1)) && ~isnan(iContourBetween(j,2))
            distanceToAxisBefore = double(abs(zAxis(j)-zAxisContours(iContourBetween(j,1))));
            distanceToAxisAfter = double(abs(zAxis(j)-zAxisContours(iContourBetween(j,2))));
            distance(j,1) = 1 - (distanceToAxisBefore/(distanceToAxisBefore+distanceToAxisAfter)); %the smaller the distance the bigger the influence
            distance(j,2) = 1 - (distanceToAxisAfter/(distanceToAxisBefore+distanceToAxisAfter));
        end
    end
end

function binaryImage = storeContoursOnAxis(binaryContours, binaryImage, wantedOutputIndex, contourIndexOnAxis)
    binaryImageIndex = find(wantedOutputIndex);
    for i = binaryImageIndex'
        if ~isnan(contourIndexOnAxis(i))
            binaryImage(:,:,i) = binaryContours{contourIndexOnAxis(i)};
        end
    end
end

function binaryImage = determineBitmaskBetweenAxis(binaryContours, binaryImage, wantedOutputIndex, contourIndexOnAxis, contourIndexBetweenAxis, contourDistanceToAxis)
    binaryImageIndex = find(wantedOutputIndex);
    for i = binaryImageIndex'
        if isnan(contourIndexOnAxis(i))
            binaryImage(:,:,i) = estimateBitmaskBetweenSlices(...
                binaryContours{contourIndexBetweenAxis(i,1)},...
                binaryContours{contourIndexBetweenAxis(i,2)},...
                contourDistanceToAxis(i,1), contourDistanceToAxis(i,2));
        end
    end 
end

function binaryImage = determineBitmaskAtTheBeginning(firstBinaryContour, binaryImage, wantedOutputIndex, iContourBetweenAxis)
    binaryImageIndex = find(wantedOutputIndex);
    firstBinaryDistances = (bwdist(firstBinaryContour) - bwdist(~firstBinaryContour));
    zeroBinaryDistances = firstBinaryDistances + (abs(min(firstBinaryDistances(:))) * 1.001); 
        %add one promile to make sure that the image that is one slice thickness away doesn't get
        %the single pixel on true
    
   numberOfSamplesBetweenContours = sum(iContourBetweenAxis(:,1) == 1);
   for i = 1:numberOfSamplesBetweenContours
        weightImage = (numberOfSamplesBetweenContours - i) / numberOfSamplesBetweenContours;
        weithtZeros = i / numberOfSamplesBetweenContours;
        binaryImage(:,:,binaryImageIndex(1) - i) = ...
            (firstBinaryDistances*weightImage +...
            zeroBinaryDistances*weithtZeros ) < 0 ;
    end
end

function binaryImage = determineBitmaskAtTheEnd(lastBinaryContour, binaryImage, wantedOutputIndex, iContourBetweenAxis)
    binaryImageIndex = find(wantedOutputIndex);
    lastBinaryDistances = (bwdist(lastBinaryContour) - bwdist(~lastBinaryContour));
    zeroBinaryDistances = lastBinaryDistances + (abs(min(lastBinaryDistances(:)))* 1.001);
        %add one promile to make sure that the image that is one slice thickness away doesn't get
        %the single pixel on true
        
    numberOfSamplesBetweenContours = sum(iContourBetweenAxis(:,2) == max(iContourBetweenAxis(:,2)));
   for i = 1:numberOfSamplesBetweenContours
        weightImage = (numberOfSamplesBetweenContours - i) / numberOfSamplesBetweenContours;
        weithtZeros = i / numberOfSamplesBetweenContours;
        binaryImage(:,:,binaryImageIndex(end) + i) = ...
            (lastBinaryDistances*weightImage +...
            zeroBinaryDistances*weithtZeros ) < 0 ;
    end
end

function bitmask = estimateBitmaskBetweenSlices(bitmaskLeft, bitmaskRight, weigthLeft, weightRigth)
%use bwdist to determine the certainty of zeros (+values) and ones (-values) for each slice
%use the weighting factor to account for the distance from the current slice to the adjacent slices.
    distancesLeft = (bwdist(bitmaskLeft) - bwdist(~bitmaskLeft)) * weigthLeft;
    distancesRight = (bwdist(bitmaskRight) - bwdist(~bitmaskRight)) * weightRigth;
    bitmask = distancesLeft+distancesRight < 0;
end