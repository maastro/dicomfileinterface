function [ voiImage ] = createImageDataForVoi(voi, image)
%CREATEIMAGEDATAFORVOI creates an image with values for a bitmask.
%
%createImageDataForVoi(voi, image) default mode. provide a VolumeOfInterest and an Image
% with loaded pixel data
%
% See also: VOLUMEOFINTEREST, CREATEIMAGEDATAFORVOIFULLGRID
if voi.columns ~= image.columns || voi.slices ~= image.slices || voi.rows ~= image.rows || ...
        voi.pixelSpacingX ~= image.pixelSpacingX || voi.pixelSpacingY ~= image.pixelSpacingY || voi.pixelSpacingZ ~= image.pixelSpacingZ
    
    throw(MException('createImageDataForVoi:InvalidInput', 'provided voi and image are not represented on the same grid!'));
end

logger = logging.getLogger('dicom-file-interface');
logger.debug('creating an image value object for VolumeOfInterest');

pixelData = NaN(size(voi.pixelData));
partOfImage = image.pixelData(voi.xCompressed, voi.yCompressed, voi.zCompressed);
pixelData(voi.pixelData) = partOfImage(voi.pixelData);

logger.debug(['dimension of voi image is ' num2str(size(pixelData))]);

voiImage = Image(voi.pixelSpacingX, ...
    voi.pixelSpacingY, ...
    voi.pixelSpacingZ, ...
    voi.realX(voi.xCompressed), ...
    voi.realY(voi.yCompressed), ...
    voi.realZ(voi.zCompressed), ...
    pixelData);
end