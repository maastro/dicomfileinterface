function [ voiImage ] = createImageDataForVoiFullGrid(voi, image)
%CREATEIMAGEDATAFORVOIFULLGRID creates an image with values for a bitmask.
%
%createImageDataForVoi(voi, refImage) can force the output image to be on the full
% grid instead of the compressed grid of volume of interest
%
% See also: VOLUMEOFINTEREST, CREATEIMAGEDATAFORVOI

logger = logging.getLogger('dicom-file-interface');
logger.debug('creating an image value object for VolumeOfInterest on full grid size');

image = voi.uncompressedPixelData .* image.pixelData;
image(image == 0) = NaN;

voiImage = Image(voi.pixelSpacingX, ...
    voi.pixelSpacingY, ...
    voi.pixelSpacingZ, ...
    voi.realX, ...
    voi.realY, ...
    voi.realZ, ...
    image);
end