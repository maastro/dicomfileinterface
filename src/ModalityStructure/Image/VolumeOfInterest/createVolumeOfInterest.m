function voi = createVolumeOfInterest(contour, refImage)
%CREATEVOLUMEOFINTEREST for a contour object on a referenced image grid
%
% image = createVolumeOfInterest(contour, refImage) creates an VolumeOfInterest object.
%  
% pixel definition documentation 
%  ftp://dicom.nema.org/MEDICAL/dicom/2015b/output/chtml/part03/sect_C.7.6.2.html
%
% See also: VOLUMEOFINTEREST, IMAGE, CREATEEMPTYIMAGEFROMCONTOUR, MATCHIMAGEREPRESENTATION,
% POLY2MASK
    if ~isa(contour, 'Contour') || ~isa(refImage, 'Image')
        throw(MException('createVolumeOfInterest:InvalidInput','please provide a valid Contour and Image object as input'));
    end

    if ~refImage.is3d
        throw(MException('createVolumeOfInterest:InvalidInput','function is made for 3d interpolation, could not project contour on image'));
    end
    
    if isempty(contour.dicomHeader) || isempty(contour.contourSlices(1).coordinateMatrix)
        throw(MException('createVolumeOfInterest:InvalidInput','provided contour is not a valid contour, no contourSlices found in object'))
    end
    
    logger = logging.getLogger('dicom-file-interface');
    logger.debug(['creating volume of interest for ' contour.name]);
    
    voi = VolumeOfInterest();
    voi = voi.copyImageProperties(refImage);

    if contour.isClosedInY
        pixelData = convertCoordinatesToBitmask({contour.contourSlices.coordinateMatrix}, voi.realX, voi.realZ, voi.realY);
    else
        logger.debug('multiple delineations in a single contour detected, splitting the bitmask conversion')
        pixelData = false(length(refImage.realX),length(refImage.realZ),length(refImage.realY));
        for i = 1:(length(contour.startNewGroup)-1)
            subSlices = contour.contourSlices(contour.startNewGroup(i):contour.startNewGroup(i+1)-1);
            subPixelData = convertCoordinatesToBitmask({subSlices.coordinateMatrix}, voi.realX, voi.realZ, voi.realY);
            pixelData = pixelData | subPixelData;
        end
    end
    voi = voi.addPixelData(permute(pixelData,[2 3 1]));
end