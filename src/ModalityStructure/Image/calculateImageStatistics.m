function out = calculateImageStatistics(image, operation)
%CALCULATEIMAGESTATISTICS can calculate statistics for an Image object.
%
% out = calculateImageStatistics(image, operation) applies to operation to the provided image object
%  valid operations are: mean, max, min, sdt, median, sum
%
% See also: Image, VolumeOfInterest

if ~isa(image, 'Image') || isempty(image.pixelData)
    throw(MException('calculateImageStatistics:InvalidInput', 'please provide image object with pixel data'))
end

switch strtrim(lower(operation))
    case 'mean'
        out = mean(image.pixelData(~isnan(image.pixelData)));
    case 'max'
        out = max(image.pixelData(~isnan(image.pixelData)));
    case 'min'
        out = min(image.pixelData(~isnan(image.pixelData)));
    case 'std'
        out = std(image.pixelData(~isnan(image.pixelData)));
    case 'median'
        out = median(image.pixelData(~isnan(image.pixelData)));
    case 'sum'
        out = sum(image.pixelData(~isnan(image.pixelData)));
    otherwise
        throw(MException('calculateImageStatistics:InvalidInput', ['invalid operation: ' operation]));
end
end