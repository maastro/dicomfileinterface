function image = matchImageRepresentation(image, refImage, defaultValue, method)
%MATCHIMAGEREPRESENTATION applies a interpolation method to resample an image onto the grid of a
%reference image
%
% image = matchImageRepresentation(image, refImage) assumes a linear interpolation with a default
%  value of 0 for empty voxels
%
% image = matchImageRepresentation(image, refImage, defaultValue) use to set a different default
%  value. Warning! The function does not check if defaultValue is valid.
%
% image = matchImageRepresentation(image, refImage, defaultValue, method) use to set a different
%  default value and interpolation method. Warning! The function does not check if interpolation method is valid.
%
% See also: IMAGE, VOLUMEOFINTEREST

if nargin < 3
    defaultValue = double(0);
end

if nargin < 4
    method = 'linear';
end

try
    newImage = interp3(...
        double(image.realY),...
        double(image.realX),...
        double(image.realZ),...
        double(image.pixelData),...
        double(refImage.realY),...
        double(refImage.realX),...
        double(refImage.realZ)',...
        ... %do not know why this Z should be rotated but the calculation won't work otherwise
        method,defaultValue);
catch exception
    newException = MException('matchImageRepresentation:ProcessingError', 'Could not perform interp3 with current settings, more details in cause of the exception');
    newException.addCause(exception);
    throw(newException);
end

image = image.copyImageProperties(refImage);

if isa(image,'VolumeOfInterest')
    newImage(newImage >= 0.5) = 1;
    newImage(newImage < 0.5) = 0;
    image = image.addPixelData(newImage);
else
    image = image.addPixelData(newImage);
end
end