classdef RtDose < DicomObj
    %RTDOSE represents a RTDOSE DicomObj
    %
    %CONSTRUCTOR
    % this = RtDose(dicomItem) creates a RtDose object
    %  using the full file path (or a DicomObj)
    %
    % See also: DICOMOBJ, CREATEIMAGEFROMRTDOSE, CREATERESCALEDIMAGEFROMRTDOSES
    
    properties
        is3dDose
        is2dDose
        isDvh
        coordinateSystem
        
        doseGridScaling
        doseUnits
        doseType
        doseSummationType
        planId
        numberOfFrames
        originX
        originY
        originZ
        gridFrameOffsetVector
        referencedRtPlanUid
        beam
        fraction
        scaledImageData
        
        realX
        realY
        realZ
    end
    
    methods
        function this = RtDose(dicomObj)
            if nargin == 0 %preserve standard empty constructor
                return;
            end
            
            if ~isa(dicomObj, 'DicomObj') || ~strcmpi('rtdose', dicomObj.modality)
                throw(MException('RtDose:constructor:InvalidInput', 'please provide a valid DicomObj when constructing a RtDose'));
            end
            
            this = dicomObj.copyObj(this);
        end
        
        function this = readDicomData(this)
            %READDICOMDATA() is a overwritten function for RtDose to apply RtDose specific
            % conversions to the raw pixel data
            this = readDicomData@DicomObj(this);
            this.pixelData(:,:,:,:) = this.pixelData(end:-1:1,:,:,:);
            this.pixelData = permute(this.pixelData,[2 4 3 1]);
        end
        
        function [roiNames] = listAvailableDvhs(this, rtstruct)
            if ~this.isDvh
                throw(MException('RtDose:listAvailableDvhs:DataNotAvailable','RtDose file does not contain any dvh data'));
            end
            
            namesInRtstruct = cell(0,0);
            for i = 1:length(rtstruct.contourNames)
                header = dicomHeaderForRoiName(rtstruct, rtstruct.contourNames{i});
                namesInRtstruct{header.ROINumber} = header.ROIName;
            end
            
            items = fieldnames(this.dicomHeader.DVHSequence);
            for i = 1:length(items)
                items2 = fieldnames(this.dicomHeader.DVHSequence.(items{i}).DVHReferencedROISequence);
                for j = 1:length(items2)
                    roiNames{i,j} = namesInRtstruct{this.dicomHeader.DVHSequence.(items{i}).DVHReferencedROISequence.(items2{j}).ReferencedROINumber}; %#ok<AGROW>
                end
            end
        end
        
        % -------- START GETTERS/SETTERS ----------------------------------
        function out = get.scaledImageData(this)
            out = double(this.pixelData) .* this.doseGridScaling;
        end
        
        function out = get.referencedRtPlanUid(this)
            out = [];
            if isfield(this.dicomHeader, 'ReferencedRTPlanSequence')
                out = this.dicomHeader.ReferencedRTPlanSequence.Item_1.ReferencedSOPInstanceUID;
            end
        end
        
        function out = get.is3dDose(this)
            out = false;
            if isfield(this.dicomHeader, 'NumberOfFrames') && ~isempty(this.dicomHeader.NumberOfFrames)
                out = true;
            end
        end
        
        function out = get.is2dDose(this)
            out = false;
            if isfield(this.dicomHeader, 'DoseSummationType') && (strcmpi(this.dicomHeader.DoseSummationType,'beam'))
                out = true;
            end
        end
        
        function out = get.isDvh(this)
            out = false;
            if isfield(this.dicomHeader,'DVHSequence')
                out = true;
            end
        end
        
        %because we do not support 2D images, the system is always i
        %should be implemented when added 2D (multiframe) support
        function out = get.coordinateSystem(~)
            out = 'i';
        end
        
        function out = get.doseGridScaling(this)
            out = [];
            if isfield(this.dicomHeader, 'DoseGridScaling')
                out = this.dicomHeader.DoseGridScaling;
            end
        end
        
        function out = get.doseUnits(this)
            out = [];
            if isfield(this.dicomHeader, 'DoseUnits')
                out = lower(this.dicomHeader.DoseUnits);
                if strcmp(out, 'absolute') %MAASTRO compatibility
                    out = 'gy';
                end
            end
        end
        
        function out = get.doseType(this)
            out = [];
            if isfield(this.dicomHeader, 'DoseType')
                out = lower(this.dicomHeader.DoseType);
            end
        end
        
        function out = get.doseSummationType(this)
            out = [];
            if isfield(this.dicomHeader, 'DoseSummationType')
                out = lower(this.dicomHeader.DoseSummationType);
            end
        end
        
        function out = get.planId(this)
            out = [];
            if isfield(this.dicomHeader, 'SeriesDescription')
                out = lower(this.dicomHeader.SeriesDescription);
            end
        end
        
        function out = get.numberOfFrames(this)
            out = [];
            if isfield(this.dicomHeader, 'NumberOfFrames')
                out = lower(this.dicomHeader.NumberOfFrames);
            end
        end
        
        function out = get.beam(this)
            out = [];
            if isfield(this.dicomHeader, 'ReferencedRTPlanSequence')
                out = this.dicomHeader.ReferencedRTPlanSequence.Item_1.ReferencedFractionGroupSequence.Item_1.ReferencedBeamSequence.Item_1.ReferencedBeamNumber;
            elseif isfield(this.dicomHeader, 'InstanceNumber')
                %awesome DGRT logic, don't blame me.
                iNumb = num2str(this.dicomHeader.InstanceNumber);
                if iNumb>=3
                    out = str2double(iNumb(1:end-2));
                end
            end
        end
        
        function out = get.fraction(this)
            out = [];
            if isfield(this.dicomHeader, 'SeriesNumber')
                out = lower(this.dicomHeader.SeriesNumber);
            end
        end
        
        function out = get.originX(this)
            if this.imageOrientationPatient(1) == 1
                out = this.x;
            elseif this.imageOrientationPatient(1) == -1
                out = this.x - ...
                    (this.pixelSpacing(1) * this.columns);
            else
                out = [];
                warning('unsupported ImageOrientationPatient detected, cannot provide origin');
            end
        end
        
        function out = get.originY(this)
            out = this.y;
        end
        
        function out = get.originZ(this)
            if this.imageOrientationPatient(5) == -1
                out = -this.z;
            elseif this.imageOrientationPatient(5) == 1
                out = -this.z - ...
                    (this.pixelSpacing(2) * (this.rows - 1));
            else
                out = [];
                warning('unsupported ImageOrientationPatient detected, cannot provide origin');
            end
        end
        
        function out = get.gridFrameOffsetVector(this)
            out = [];
            if isfield(this.dicomHeader, 'GridFrameOffsetVector')
                out = this.dicomHeader.GridFrameOffsetVector'/10;
            end
        end
        
        function out = get.realX(this)
            out = (this.originX : this.pixelSpacing(1) : (this.originX+(this.columns-1)*this.pixelSpacing(1)) )';
        end
        
        function out = get.realY(this)
            out = (this.originY + this.gridFrameOffsetVector)';
        end
        
        function out = get.realZ(this)
            out = (this.originZ : this.pixelSpacing(2) : (this.originZ+(this.rows-1)*this.pixelSpacing(2)) )';
        end
    end
end