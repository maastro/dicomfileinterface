function [binData, sumDoseGridScaling] = combineDosePixelData(rtDoses)
%COMBINEDOSEPIXELDATA creates new pixelData combining the list of RtDose files
%
% [binData, sumDoseGridScaling] = combineDosePixelData(rtDoses) combines the pixel data
%   of RtDose files which are based on the same dose grid. binData is a uint16 matrix that can be
%   converted to dose in [Gy] by using the doseGridScaling factor. these values can be used to write
%   a new combined RtDose file.
%
% See also: RTDOSE, DICOMOBJ, DICOMOBJ.WRITETOFILE, CREATEDOSESERIESFORCOMBINABLEPLANS
    logger = logging.getLogger('dicom-file-interface');
    logger.debug('combining RtDose images')

    if ~doseGridsAreEqual(rtDoses)
        throw(MException('combineDosePixelData:InvalidInput', 'provided RtDose array are not based on the same reference spacing'));
    end
    
    if dosesAreNotAbsolute(rtDoses)
        throw(MException('combineDosePixelData:InvalidInput', 'provided RtDose array contains one ore more relative RtDose objects, cannot add'));
    end

    sumImage = zeros(rtDoses(1).rows,rtDoses(1).columns,1,rtDoses(1).numberOfFrames);
    sumDoseGridScaling = 0;
    for rtDose = rtDoses
        pixelData = dicomread(rtDose.filename);
        sumImage = sumImage + double(pixelData).*rtDose.doseGridScaling;
        sumDoseGridScaling = sumDoseGridScaling + rtDose.doseGridScaling;
    end
    binData = uint32(sumImage./sumDoseGridScaling);
end


function out = doseGridsAreEqual(doses)
    out = isequal(doses.frameOfReferenceUid) && ...
        isequal(doses.originX) && ...
        isequal(doses.originY) && ...
        isequal(doses.originZ) && ...
        isequal(doses.imageOrientationPatient) && ...
        isequal(doses.imagePositionPatient);
end

function out = dosesAreNotAbsolute(doses)
    out = false;
    for i = 1:length(doses)
        if strcmp(doses(i).doseUnits, 'relative')
            out = true;
            break;
        end
    end
end
