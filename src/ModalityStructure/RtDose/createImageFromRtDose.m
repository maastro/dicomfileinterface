function image = createImageFromRtDose(rtdose)
%CREATEIMAGEFROMRTDOSE creates an Image object using the binary data of the RtDose file
%
% image = createImageFromRtDose(rtdose)
%
% See also: RTDOSE, IMAGE
logger = logging.getLogger('dicom-file-interface');
logger.debug('converting RtDose pixelData to an Image object');

if isempty(rtdose.pixelData)
    throw(MException('createImageFromRtDose:InvalidInput', 'Invalid RtDose, please read the dicomdata before using this function'));
end

if ~isequal(rtdose.imageOrientationPatient,...
        [1;0;0;0;1;0]);
    throw(MException('createImageFromRtDose:ProcessingError', ['Unsupported ImagePositionPatient for provided rtdose: ' num2str(rtdose.imageOrientationPatient')]));
end


if ~strcmp(rtdose.doseUnits, 'gy')
    throw(MException('createImageFromRtDose:InvalidInput', ['Invalid RtDose, this functions only accepts dose units in Gy, for this file its: ' rtdose.doseUnits]));
end

ySpacing = abs(rtdose.realY(1) - rtdose.realY(2)); %assuming uniform pixel slice spacing
image = Image(rtdose.pixelSpacing(1), ySpacing, rtdose.pixelSpacing(2), rtdose.realX, rtdose.realY, rtdose.realZ, squeeze(rtdose.scaledImageData));
end

