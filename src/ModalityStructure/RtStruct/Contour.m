classdef Contour
    %CONTOUR represents a singe contour of an RtStruct.
    %
    %CONSTRUCTOR
    % this = Contour(dicomHeader) creates a contour object of the parsed dicom header created with
    % dicomHeaderForRoiName or dicomHeaderForRoiNumber function
    %
    % See also: RTSTRUCT, CONTOURSLICE, CREATECONTOUR, DICOMHEADERFORROINAME, DICOMHEADERFORROINUMBER
    
    properties
        dicomHeader
        number
        name
        contourSlices = ContourSlice()
        closedPlanar
        coPlanar
        singlePoint
        forced
        relativeElectronDensity
        numberOfContourSlices
        numberOfCtSlices
        volume
        colorRgb
        referencedFrameOfReferenceUid
        y
        uniqueY
        indexUniqueY
        lowerX
        lowerY
        lowerZ
        upperX
        upperY
        upperZ
        
        isClosedInY
        startNewGroup
    end
    
    methods
        function this = Contour(dicomHeader)
            if nargin == 0 %preserve standard empty constructor
                return;
            end
            this = this.parseDicomHeader(dicomHeader);
        end
        
        function this = parseDicomHeader(this, dicomHeader)
        %PARSEDICOMHEADER(header) parses the information of the contour specific header information
        % to create ContourSlice objects that build the contour        
            if ~isa(dicomHeader, 'struct') || ~isfield(dicomHeader, 'ContourSequence')
                throw(MException('Contour:parseDicomHeader:InvalidInput', 'Provided input is not a valid dicom header, please use dicomHeaderForRoiName or -Number.m'));
            end
            this.dicomHeader = dicomHeader;
            
            items = fieldnames(dicomHeader.ContourSequence);
            this.contourSlices(1:length(items)) = ContourSlice();
            for i = 1:length(items)
                this.contourSlices(i) = ContourSlice(dicomHeader.ContourSequence.(items{i}));
            end
        end
        
        % -------- START GETTERS/SETTERS ----------------------------------
        function out = get.name(this)
            out = 'N/A';
            if isfield(this.dicomHeader, 'ROIName')
                out = this.dicomHeader.ROIName;
            end
        end
        
        function out = get.number(this)
            out = 'N/A';
            if isfield(this.dicomHeader, 'ROINumber')
                out = this.dicomHeader.ROINumber;
            end
        end
        
        function out = get.referencedFrameOfReferenceUid(this)
            out = [];
            if isfield(this.dicomHeader, 'ReferencedFrameOfReferenceUID')
                out = this.dicomHeader.ReferencedFrameOfReferenceUID;
            end
        end
        
        function out = get.numberOfContourSlices(this)
            out = length(this.contourSlices);
        end
        
        function out = get.numberOfCtSlices(this)
            out = length(this.uniqueY);
        end
        
        function out = get.closedPlanar(this)
            out = true;
            for i = 1:this.numberOfContourSlices
                if ~this.contourSlices(i).closedPlanar
                    out = false;
                    return;
                end
            end
        end
        
        function out = get.coPlanar(this)
            out = false;
            for i = 1:this.numberOfContourSlices
                if ~this.contourSlices(i).coPlanar
                    out = true;
                    return;
                end
            end 
        end
        
        function out = get.singlePoint(this)
            out = true;
            for i = 1:this.numberOfContourSlices
                if ~this.contourSlices(i).singlePoint
                    out = false;
                    return;
                end
            end
        end
        
        function out = get.colorRgb(this)
            if isfield(this.dicomHeader, 'ROIDisplayColor')
                out = this.dicomHeader.ROIDisplayColor./256;
            else
                %when no color settings are available set to white.
                out = [1 1 1];
            end
        end
        
        function out = get.volume(this)
            out = NaN;
            if isfield(this.dicomHeader, 'ROIVolume')
                out = this.dicomHeader.ROIVolume;
            end
        end
        
        function out = get.forced(this)
            out = true;
            if isempty(this.relativeElectronDensity)
                out = false;
            end
        end
        
        function out = get.relativeElectronDensity(this)
            out = [];
            if isfield(this.dicomHeader, 'ROIPhysicalPropertiesSequence');
                items = fieldnames(this.dicomHeader, 'ROIPhysicalPropertiesSequence');
                for i = 1:length(items)
                    out(i) = this.dicomHeader.ROIPhysicalPropertyValue.(items{1}).ROIPhysicalPropertyValue; %#ok<AGROW>
                end
            end
        end
        
        function out = get.y(this)
            out = zeros(this.numberOfContourSlices,1);
            for i = 1:this.numberOfContourSlices
                out(i) = NaN;
                if ~isempty(this.contourSlices(i).y)
                    out(i) = this.contourSlices(i).y(1);
                end
            end
        end
        
        function out = get.uniqueY(this)
            [out, ~, ~] = unique(this.y);
        end
        
        function out = get.indexUniqueY(this)
            [~, ~, out] = unique(this.y);
        end
        
        function out = get.lowerX(this)
            out = min(this.contourSlices(1).x);
            for i = 2:this.numberOfContourSlices
                out = min([out, min(this.contourSlices(i).x)]);
            end
        end
        
        function out = get.lowerY(this)
            out = min(this.y);
        end
        
        function out = get.lowerZ(this)
            out = min(this.contourSlices(1).z);
            for i = 2:this.numberOfContourSlices
                out = min([out, min(this.contourSlices(i).z)]);
            end
        end
        
        function out = get.upperX(this)
            out = max(this.contourSlices(1).x);
            for i = 2:this.numberOfContourSlices
                out = max([out, max(this.contourSlices(i).x)]);
            end
        end
        
        function out = get.upperY(this)
            out = max(this.y);
        end
        
        function out = get.upperZ(this)
            out = max(this.contourSlices(1).z);
            for i = 2:this.numberOfContourSlices
                out = max([out, max(this.contourSlices(i).z)]);
            end
        end

        function out = get.isClosedInY(this)
            difference = diff(int32(this.uniqueY.*1000));
            out = min(difference== difference(1));
        end
        
        function out = get.startNewGroup(this)
            %assumption: In current clinical practice exported contours are always projected to
            %   every slice.
            if this.isClosedInY
                out = [1, this.numberOfCtSlices];
            else
                difference = abs(diff(int32(this.y.*1000)));
                threshold = min(diff(int32(this.uniqueY.*1000)));
                out = [1, (find((difference - threshold) > 0)+1)', this.numberOfContourSlices+1];
            end
        end
    end
end