classdef ContourSlice
    %CONTOURSLICE contains the information of a single ContourSlice
    %
    % CONSTRUCTOR
    %  this = ContourSlice(contourSequenceItem) translates the information of a single item in the
    %  (3006,0039) ROIContourSequence dicom tag
    %
    % See also: CONTOUR, CTSLICE
    properties
        x %in cm
        y %in cm
        z %in cm
        coordinateMatrix
        
        referencedSopInstanceUid
        referencedSopClassUid
        
        closedPlanar
        coPlanar
        singlePoint
    end
    
    methods
        function this = ContourSlice(contourSequenceItem)
            if nargin == 0 %preserve standard empty constructor
                return;
            end
            
            this = this.parseDicomHeader(contourSequenceItem);
        end
        
        function this = parseDicomHeader(this, contourSequenceItem)
            %PARSEDICOMHEADER this function parses the provided dicomheader and creates a contour slice
            this.x   =   contourSequenceItem.ContourData(1:3:end)/10;
            this.y   =   contourSequenceItem.ContourData(3:3:end)/10;
            this.z   =   -contourSequenceItem.ContourData(2:3:end)/10;
            
            this.referencedSopInstanceUid = contourSequenceItem.ContourImageSequence.Item_1.ReferencedSOPClassUID;
            this.referencedSopClassUid = contourSequenceItem.ContourImageSequence.Item_1.ReferencedSOPClassUID;
            
            this = this.setPlanarStatus(contourSequenceItem.ContourGeometricType);
        end
        
        % -------- START GETTERS/SETTERS ----------------------------------
        function this = set.referencedSopInstanceUid(this, uid)
            if ~ischar(uid)
                throw(MException('ContourSlice:set:referencedSopInstanceUid:InvalidInput', 'invalid uid for referencedSopInstanceUid'))
            else
                this.referencedSopInstanceUid = uid;
            end
        end
        
        function this = set.referencedSopClassUid(this, uid)
            if ~ischar(uid)
                throw(MException('ContourSlice:set:referencedSopInstanceUid:referencedSopClassUid', 'invalid uid for referencedSopClassUid'))
            else
                this.referencedSopClassUid = uid;
            end
        end
        
        function out = get.coordinateMatrix(this)
            out = [];
            if ~isempty(this.x)
                out = [this.x, this.z, this.y];
            end
        end
    end
    
    methods (Access = private)
        %http://dicom.nema.org/medical/dicom/2016c/output/chtml/part03/sect_C.8.8.6.html#sect_C.8.8.6.1
        function this = setPlanarStatus(this, geometricTypeTag)
            switch geometricTypeTag
                case 'POINT'
                    this.closedPlanar = true;
                    this.coPlanar = false;
                    this.singlePoint = true;
                case 'OPEN_PLANAR'
                    this.closedPlanar = false;
                    this.coPlanar = true;
                    this.singlePoint = false;
                case 'OPEN_NONPLANAR'
                    this.closedPlanar = false;
                    this.coPlanar = false;
                    this.singlePoint = false;
                case 'CLOSED_PLANAR'
                    this.closedPlanar = true;
                    this.coPlanar = true;
                    this.singlePoint = false;
            end
        end
    end
end