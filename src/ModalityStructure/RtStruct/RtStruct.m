classdef RtStruct < DicomObj
    %RTSTRUCT Representation of a DICOM RTSTRUCT
    %
    %CONSTRUCTOR
    % this = RtStruct(dicomItem) creates a RtStruct object
    %  using the full file path (or a DicomObj)
    %
    % See also: DICOMOBJ, CONTOUR, CONTOURSLICE, DICOMHEADERFORROINUMBER, DICOMHEADERFORROINAME,
    % DICOMINFOWRAPPER
    
    properties
        structureSetSequence
        observationSequence
        contourSequence
        contourNames
        referencedImageSeriesUid
    end
    
    methods
        function this = RtStruct(dicomObj)
            if nargin == 0 %preserve standard empty constructor
                return;
            end
            
            if ~isa(dicomObj, 'DicomObj') || ~strcmpi('rtstruct', dicomObj.modality)
                throw(MException('RtStruct:constructor:InvalidInput', 'please provide a valid DicomObj when constructing a RtStruct'));
            end
            
            this = dicomObj.copyObj(this);
        end

        function readDicomData(~)
        %READDICOMDATA is overwritten for RTSTRUCT because RTSTRUCTS do not contain pixel data, just
        % header info.
            warning('this standard dicom function is overwritten because the rtstruct dicom object does not contain an image block');
        end
                
        % -------- START GETTERS/SETTERS ----------------------------------
        function out = get.structureSetSequence(this)
            out = this.dicomHeader.StructureSetROISequence;
        end
        
        function out = get.observationSequence(this)
            out = this.dicomHeader.RTROIObservationsSequence;
        end
        
        function out = get.contourSequence(this)
            out = this.dicomHeader.ROIContourSequence;
        end
        
        function out = get.contourNames(this)
            items = fieldnames(this.structureSetSequence);
            out = cell(length(items), 1);
            for i = 1:length(items)
                out{this.structureSetSequence.(items{i}).ROINumber} = this.structureSetSequence.(items{i}).ROIName;
            end
        end
        
        function out = get.referencedImageSeriesUid(this)
            out = [];
            if isfield(this.dicomHeader, 'ReferencedFrameOfReferenceSequence')
                referencedFrameOfReferenceSequence = this.dicomHeader.ReferencedFrameOfReferenceSequence.Item_1;
                if isfield(referencedFrameOfReferenceSequence, 'RTReferencedStudySequence')
                    studySequence = referencedFrameOfReferenceSequence.RTReferencedStudySequence.Item_1;
                    if isfield(studySequence, 'RTReferencedSeriesSequence')
                        rtReferenceSeriesSequence = studySequence.RTReferencedSeriesSequence.Item_1;
                        if isfield(rtReferenceSeriesSequence, 'SeriesInstanceUID')
                            out = rtReferenceSeriesSequence.SeriesInstanceUID;
                        end
                    end
                end
            end
        end
    end
end