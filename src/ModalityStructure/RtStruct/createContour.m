function contour = createContour(rtStruct, wantedRoi)
%CREATECONTOUR creates a Contour object for the provided number or name
%
% contour = createContour(rtStruct, wantedRoi)
%
% See also: CONTOUR, DICOMHEADERFORROINUMBER, DICOMHEADERFORROINAME
logger = logging.getLogger('dicom-file-interface');

if isnumeric(wantedRoi)
    dicomHeader = dicomHeaderForRoiNumber(rtStruct, wantedRoi);
    logger.debug(['Creating contour object for roi number' num2str(wantedRoi)]);
elseif ischar(wantedRoi)
    dicomHeader = dicomHeaderForRoiName(rtStruct, wantedRoi);
    logger.debug(['Creating contour object for roi ' wantedRoi]);
else
    throw(MException('createContour:InvalidInput', ['invalid roi input, provide a number or a name: ' num2str(wantedRoi)]))
end

contour = Contour(dicomHeader);
end