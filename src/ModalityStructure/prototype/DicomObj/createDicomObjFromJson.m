function dicomObj = createDicomObjFromJson(json)
%CREATEDICOMOBJFROMJSON 
    %json input can be a fullfilename of json string
    dicomObj = DicomObj();
    jsonStruct = loadjson(json);
    dicomObj.dicomHeader = jsonStruct.dicomObjJson.dicomHeader;
    dicomObj.pixelData = jsonStruct.dicomObjJson.pixelData;
end

