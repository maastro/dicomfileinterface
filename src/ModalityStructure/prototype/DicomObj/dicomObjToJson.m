function json = dicomObjToJson( dicomObj )
%DICOMOBJTOJSON convert the dicomObj to json format.
    dataStruct.dicomHeader = dicomObj.dicomHeader;
    dataStruct.pixelData = dicomObj.pixelData;
    json = savejson('dicomObjJson', dataStruct, 'FloatFormat', '%.14g');
end

