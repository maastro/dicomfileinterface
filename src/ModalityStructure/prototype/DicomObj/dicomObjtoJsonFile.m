function [jsonStr, jsonFile] = dicomObjtoJsonFile( dicomObj, outputDir )
%DICOMOBJTOJSONFILE
    jsonFile = fullfile(outputDir, [upper(this.modality) this.sopInstanceUid '.json']);
    dataStruct.dicomHeader = dicomObj.dicomHeader;
    dataStruct.pixelData = dicomObj.pixelData;
    jsonStr = savejson('dicomObjJson', dataStruct, 'FileName', jsonFile, 'FloatFormat', '%.14g');
end

