function out = convertPathToUnix(path)
%CONVERTPATHTOUNIX helper function to convert the path to unix standard
    out = regexprep(path, '\', '/');
end