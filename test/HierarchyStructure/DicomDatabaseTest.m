classdef DicomDatabaseTest < matlab.unittest.TestCase
    
    
    properties
        TEST_DIR = './resources/maastro/';
        REF_NR_OF_TEST_FILES = uint64(139);
        REF_NR_OF_TEST_PATIENTS = uint64(1);
    end
    
    methods (Test)
        function addNewFolderToDatabaseTest(this)
            dicomDb = DicomDatabase();
            dicomDb = addNewFolderToDatabase(dicomDb, this.TEST_DIR);
            verifyEqual(this, dicomDb.filesInDb.Count, this.REF_NR_OF_TEST_FILES);
            verifyEqual(this, dicomDb.nrOfPatients, this.REF_NR_OF_TEST_PATIENTS);
        end
        
        function addNewFolderToDatabaseWrongFolder(this)
            dicomDb = DicomDatabase();
            verifyError(this, @()addNewFolderToDatabase(dicomDb, 'anyDir'), 'addNewFolderToDatabase:InvalidInput');
            verifyError(this, @()dicomDb.getPatientObject('anyPatient'), 'DicomDatabase:getPatientObject:DataNotAvailable');
        end
    end
end