classdef PatientTest < matlab.unittest.TestCase
    
    properties
        RTSTRUCT_FILE = './resources/maastro/RTSTRUCT.9999.70989091007225463773458855903144372742.dcm';
        RTDOSE_FILE = './resources/maastro/RTDOSE.9999.245420512207561792050922341714899059838.dcm';
        RTPLAN_FILE = './resources/maastro/RTPLAN.9999.42722492099431225192788770082885116854.dcm';
        SINGLE_CT_FILE = './resources/maastro/ct/CT.9999.5421481024749737124441004800158416632.dcm';
        
        REF_RTDOSE_SOPINSTANCEUID = '9999.245420512207561792050922341714899059838';
        REF_RTSTRUCT_SOPINSTANCEUID = '9999.70989091007225463773458855903144372742';
        REF_RTPLAN_SOPINSTANCEUID = '9999.42722492099431225192788770082885116854';
        REF_CTSCAN_SERIESINSTANCEUID = '9999.222499342266219596535467323678278275423';
        
        REF_PLAN_LABEL = '1p1b1d1a';
        
        patient;
    end
    
    methods (TestClassSetup)
        function setupOnce(this)
            this.patient = Patient();
            this.patient = this.patient.parseDicomObj(DicomObj(this.RTPLAN_FILE));
            this.patient = this.patient.parseDicomObj(DicomObj(this.RTSTRUCT_FILE));
            this.patient = this.patient.parseDicomObj(DicomObj(this.RTDOSE_FILE));
            this.patient = this.patient.parseDicomObj(DicomObj(this.SINGLE_CT_FILE));
        end
    end
    
    methods (Test)
        %--- happy path tests ----%
        function getCtScanForPlanTest(this)
            ctScan = getCtScanForPlan(this.patient, this.REF_RTPLAN_SOPINSTANCEUID);
            verifyEqual(this, ctScan.seriesInstanceUid, this.REF_CTSCAN_SERIESINSTANCEUID);
        end
        
        function getRtDoseForPlanTest(this)
            rtDose = getRtDoseForPlan(this.patient, this.REF_RTPLAN_SOPINSTANCEUID);
            verifyEqual(this, rtDose.sopInstanceUid, this.REF_RTDOSE_SOPINSTANCEUID);
        end
        
        function getRtStructForPlanTest(this)
            rtStruct = getRtStructForPlan(this.patient, this.REF_RTPLAN_SOPINSTANCEUID);
            verifyEqual(this, rtStruct.sopInstanceUid, this.REF_RTSTRUCT_SOPINSTANCEUID);
        end
        
        function createPlanPackageTest(this)
            [rtPlan, rtDose, rtStruct, ctScan] = createPlanPackage(this.patient, this.REF_PLAN_LABEL);
            verifyEqual(this, rtPlan.sopInstanceUid, this.REF_RTPLAN_SOPINSTANCEUID);
            verifyEqual(this, rtDose.sopInstanceUid, this.REF_RTDOSE_SOPINSTANCEUID);
            verifyEqual(this, rtStruct.sopInstanceUid, this.REF_RTSTRUCT_SOPINSTANCEUID);
            verifyEqual(this, ctScan.seriesInstanceUid, this.REF_CTSCAN_SERIESINSTANCEUID);
        end
        
        %--- unhappy path tests ----%
        function patientInvalidInputTest(this)
            errorPatient = Patient();
            verifyError(this, @()errorPatient.parseDicomObj(double(1)), 'Patient:parseDicomObj:InvalidInput');
            verifyError(this, @()errorPatient.getDicomObjectSeries('InvalidUid'), 'Patient:getDicomObjectSeries:DataNotAvailable')
            verifyError(this, @()errorPatient.getDicomObjectStudy('InvalidUid'), 'Patient:getDicomObjectStudy:DataNotAvailable')
        end
        
        function getCtScanForPlanNoRtStructTest(this)
            errorPatient = Patient();
            errorPatient = errorPatient.parseDicomObj(DicomObj(this.RTPLAN_FILE));
            errorPatient = errorPatient.parseDicomObj(DicomObj(this.SINGLE_CT_FILE));
            verifyError(this, @()getCtScanForPlan(errorPatient, this.REF_RTPLAN_SOPINSTANCEUID), 'getCtScanForPlan:DataNotAvailable');
            verifyError(this, @()createPlanPackage(errorPatient, this.REF_PLAN_LABEL), 'createPlanPackage:ProcessingError');
        end
        
        function getCtScanForPlanNoCtScanTest(this)
            errorPatient = Patient();
            errorPatient = errorPatient.parseDicomObj(DicomObj(this.RTPLAN_FILE));
            errorPatient = errorPatient.parseDicomObj(DicomObj(this.RTSTRUCT_FILE));
            verifyError(this, @()getCtScanForPlan(errorPatient, this.REF_RTPLAN_SOPINSTANCEUID), 'getCtScanForPlan:DataNotAvailable');
            verifyError(this, @()createPlanPackage(errorPatient, this.REF_PLAN_LABEL), 'createPlanPackage:ProcessingError');
        end
        
        function getRtStructForPlanWrongRtStructTest(this)
            errorPatient = Patient();
            verifyError(this, @()getRtStructForPlan(errorPatient, this.REF_RTPLAN_SOPINSTANCEUID), 'getRtStructForPlan:DataNotAvailable');
            
            errorPatient = errorPatient.parseDicomObj(DicomObj(this.RTPLAN_FILE));
            wrongRtStruct = DicomObj(this.RTSTRUCT_FILE);
            wrongRtStruct.dicomHeader.SOPInstanceUID = dicomuid;
            errorPatient = errorPatient.parseDicomObj(wrongRtStruct);
            
            verifyError(this, @()getRtStructForPlan(errorPatient, this.REF_RTPLAN_SOPINSTANCEUID), 'getRtStructForPlan:DataNotAvailable');
            verifyError(this, @()createPlanPackage(errorPatient, this.REF_PLAN_LABEL), 'createPlanPackage:ProcessingError');
        end
        
        function getRtDoseForPlanWrongDoseTest(this)
            errorPatient = Patient();
            errorPatient = errorPatient.parseDicomObj(DicomObj(this.RTPLAN_FILE));
            verifyError(this, @()getRtDoseForPlan(errorPatient, this.REF_RTPLAN_SOPINSTANCEUID), 'getRtDoseForPlan:DataNotAvailable');
            
            doseObjWrongBeamSummation = DicomObj(this.RTDOSE_FILE);
            doseObjWrongBeamSummation.dicomHeader.DoseSummationType = 'BEAM';
            doseObjWrongBeamSummation.dicomHeader.SOPInstanceUID = dicomuid;
            errorPatient = errorPatient.parseDicomObj(doseObjWrongBeamSummation);
            
            doseObjWrongManufacturor = DicomObj(this.RTDOSE_FILE);
            doseObjWrongManufacturor.dicomHeader.Manufacturer = 'MAASTRO SDT';
            doseObjWrongManufacturor.dicomHeader.SOPInstanceUID = dicomuid;
            errorPatient = errorPatient.parseDicomObj(doseObjWrongManufacturor);
            
            verifyError(this, @()getRtDoseForPlan(errorPatient, this.REF_RTPLAN_SOPINSTANCEUID), 'getRtDoseForPlan:DataNotAvailable');
            verifyError(this, @()createPlanPackage(errorPatient, this.REF_PLAN_LABEL), 'createPlanPackage:ProcessingError');
        end
    end
end