classdef SeriesTest < matlab.unittest.TestCase
    
    properties
        SINGLE_CT_FILE = './resources/maastro/ct/CT.9999.5421481024749737124441004800158416632.dcm';
        ANOTHER_CT_FILE = './resources/maastro/ct/CT.9999.8491026205003713361550921734261875908.dcm';

        REF_CTSCAN_SERIESINSTANCEUID = '9999.222499342266219596535467323678278275423';
        REF_CT_FILE_SOPINSTANCEUID = '9999.5421481024749737124441004800158416632';
        REF_ANOTHER_CT_FILE_SOPINSTANCEUID = '9999.8491026205003713361550921734261875908'; 
    end
    
    methods (TestClassSetup)
    end
    
    methods (Test)        
        %--- happy path tests ----%       
        function createSeriesFromOneObject(this)
           	series = Series(DicomObj(this.SINGLE_CT_FILE));
            series.getDicomObj(this.REF_CT_FILE_SOPINSTANCEUID);
            verifyEqual(this, series.id, this.REF_CTSCAN_SERIESINSTANCEUID);
        end
        
        function createSeriesFromTwoObject(this)
           	series = Series();
            series = series.parseDicomObj(DicomObj(this.SINGLE_CT_FILE));
            series = series.parseDicomObj(DicomObj(this.ANOTHER_CT_FILE));
            series.getDicomObjArray();
            verifyEqual(this, series.id, this.REF_CTSCAN_SERIESINSTANCEUID);
        end

        %--- unhappy path tests ----%
        function parseInvalidObj(this)
            series = Series();
            verifyError(this, @()series.parseDicomObj([]), 'Series:parseDicomObj:InvalidInput');
            verifyError(this, @()series.parseDicomObj(DicomObj()), 'Series:parseDicomObj:InvalidInput');
        end
        
        function requestUnavailableObj(this)
            series = Series();
            verifyError(this, @()series.getDicomObj(this.REF_ANOTHER_CT_FILE_SOPINSTANCEUID), 'Series:getDicomObj:DataNotAvailable');
        end
    end
    
end

