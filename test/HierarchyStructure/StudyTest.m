classdef StudyTest  < matlab.unittest.TestCase
    
    properties
        RTPLAN_FILE = './resources/maastro/RTPLAN.9999.42722492099431225192788770082885116854.dcm';
        SINGLE_CT_FILE = './resources/maastro/ct/CT.9999.5421481024749737124441004800158416632.dcm';
        ANOTHER_CT_FILE = './resources/maastro/ct/CT.9999.8491026205003713361550921734261875908.dcm';
        
        REF_CTSCAN_STUDYINSTANCEUID = '9999.176707096301547682584419484115766666734';
        REF_CTSCAN_SERIESINSTANCEUID = '9999.222499342266219596535467323678278275423';
        REF_CT_FILE_SOPINSTANCEUID = '9999.5421481024749737124441004800158416632';
        REF_ANOTHER_CT_FILE_SOPINSTANCEUID = '9999.8491026205003713361550921734261875908'; 
    end
    
    methods (Test)
        %--- happy path tests ----%       
        function createStudyFromOneObject(this)
           	study = Study(DicomObj(this.SINGLE_CT_FILE));
            study.getSeriesObject(this.REF_CTSCAN_SERIESINSTANCEUID);
            verifyEqual(this, study.id, this.REF_CTSCAN_STUDYINSTANCEUID);
        end
        
        function createStudyFromTwoObjectSameSeries(this)
           	study = Study();
            study = study.parseDicomObj(DicomObj(this.SINGLE_CT_FILE));
            study = study.parseDicomObj(DicomObj(this.ANOTHER_CT_FILE));
            study.getDicomObjArray();
            verifyEqual(this, study.id, this.REF_CTSCAN_STUDYINSTANCEUID);
            verifyEqual(this, study.nrOfSeries, uint64(1));
        end
        
        function createStudyFromTwoObjectDifferentSeries(this)
           	study = Study();
            study = study.parseDicomObj(DicomObj(this.SINGLE_CT_FILE));
            study = study.parseDicomObj(DicomObj(this.RTPLAN_FILE));
            study.getDicomObjArray();
            verifyEqual(this, study.id, this.REF_CTSCAN_STUDYINSTANCEUID);
            verifyEqual(this, study.nrOfSeries, uint64(2));
        end

        %--- unhappy path tests ----%
        function parseInvalidObj(this)
            study = Study();
            verifyError(this, @()study.parseDicomObj([]), 'Study:parseDicomObj:InvalidInput');
            verifyError(this, @()study.parseDicomObj(DicomObj()), 'Study:parseDicomObj:InvalidInput');
        end
        
        function requestUnavailableObj(this)
            study = Study();
            verifyError(this, @()study.getSeriesObject(this.REF_CTSCAN_SERIESINSTANCEUID), 'Study:getSeriesObject:DataNotAvailable');
        end
    end 
end