function results = hierarchyStructureTest()
logger = logging.getLogger('dicom-file-interface');
logger.setCommandWindowLevel(logger.OFF);

test = SeriesTest();
results = test.run;
clear test;

test = StudyTest();
results = [results test.run];
clear test;

test = PatientTest();
results = [results test.run];
clear test;

test = DicomDatabaseTest();
results = [results test.run];
clear test;
end