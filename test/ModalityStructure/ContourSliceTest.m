classdef ContourSliceTest < matlab.unittest.TestCase
    
    properties
        RTSTRUCT_FILE = './resources/maastro/RTSTRUCT.9999.70989091007225463773458855903144372742.dcm';
        ROI_NUMBER = 7;
        rtStruct;
    end
    
    methods (TestClassSetup)
        function setupOnce(this) %reading the rtstruct takes too long, put in setup
            this.rtStruct = RtStruct(DicomObj(this.RTSTRUCT_FILE));
        end
    end
    
    methods (Test)
        % --- happy path ---%
        function contourSliceTest(this)
            header = dicomHeaderForRoiNumber(this.rtStruct, this.ROI_NUMBER);
            contourSlice = ContourSlice(header.ContourSequence.Item_1);
            verifyEqual(this, contourSlice.closedPlanar, true);
        end
    end
end