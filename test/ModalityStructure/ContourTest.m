classdef ContourTest < matlab.unittest.TestCase

    properties
        RTSTRUCT_FILE = './resources/maastro/RTSTRUCT.9999.70989091007225463773458855903144372742.dcm';
        ROI_NAME = 'GTV-1';
        ROI_NUMBER = 7;
        REF_NUMBER_OF_CONTOUR_SLICES = 15;
        REF_CLOSED_PLANAR = true;
        REF_CO_PLANAR = false
        rtStruct;
    end
    
    methods (TestClassSetup)
        function setupOnce(this) %reading the rtstruct takes too long, put in setup
            this.rtStruct = RtStruct(DicomObj(this.RTSTRUCT_FILE));
        end
    end
    
    methods (Test)
        % --- happy path ---%
        function this = contourTest(this)
            contour = createContour(this.rtStruct, this.ROI_NUMBER);
            verifyEqual(this, contour.numberOfContourSlices, this.REF_NUMBER_OF_CONTOUR_SLICES);
            verifyEqual(this, contour.closedPlanar, this.REF_CLOSED_PLANAR)
            verifyEqual(this, contour.coPlanar, this.REF_CO_PLANAR)
        end
        
        % --- unhappy path ---%
        function this = createContourInvalidInputTest(this)
            verifyError(this, @()createContour(this.rtStruct, cell(0,0)), 'createContour:InvalidInput')
        end
    end
    
end

