classdef CtScanTest < matlab.unittest.TestCase
    %this test is a bit lacking, but will update after reworking the coordinate system (scheduled change)
    
    properties
        TEST_CT_SCAN_DIR = './resources/maastro/ct/'
        SINGLE_CT_FILE = './resources/maastro/ct/CT.9999.100057635262070057515533907105156738494.dcm';
        
        REF_NR_OF_SLICES = 136;
        REF_MEAN_CT_VALUES = -782.5481572;        
    end
    
    methods (Test)
        % --- happy path ---%
        function ctScanTest(this)
            ctScan = CtScan(this.TEST_CT_SCAN_DIR);
            verifyEqual(this, ctScan.numberOfSlices, this.REF_NR_OF_SLICES);
        end
        
        function createImageFromCtTest(this)
            ctScan = CtScan(this.TEST_CT_SCAN_DIR);
            ctScan = ctScan.readDicomData();
            verifyEqual(this, mean(ctScan.pixelData(:)), this.REF_MEAN_CT_VALUES, 'RelTol', 0.001);
        end        
        %--- unhappy path ---%
        function ctScanInvalidInputTest(this)
            verifyError(this, @()CtScan('./invalidDir'), 'CtScan:constructor:InvalidInput');
            verifyError(this, @()CtScan(1), 'CtScan:constructor:InvalidInput')
        end
    end
end