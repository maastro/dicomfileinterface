classdef CtSliceTest < matlab.unittest.TestCase
    
    
    properties
        SINGLE_CT_FILE = './resources/maastro/ct/CT.9999.100057635262070057515533907105156738494.dcm';
        REF_MEAN_SCALED_IMAGE = -743.42197418;
    end
    
    methods (TestClassSetup)

    end
    
    methods (Test)
        % --- happy path ---%
        function ctSliceTest(this)
            ctSlice = CtSlice(DicomObj(this.SINGLE_CT_FILE));
            verifyEqual(this, ctSlice.modality, 'ct');
        end
        
        function scaledImageTest(this)
            ctSlice = CtSlice(DicomObj(this.SINGLE_CT_FILE));
            ctSlice = ctSlice.readDicomData();
            verifyEqual(this, mean(ctSlice.scaledImageData(:)), this.REF_MEAN_SCALED_IMAGE, 'RelTol', 0.001);
        end
        
        %--- unhappy path ---%
        function ctSliceInvalidInputTest(this)
            dicomObj = DicomObj();
            verifyError(this, @()CtSlice(dicomObj), 'CtSlice:constructor:InvalidInput');
            
            dicomObj = dicomObj.readDicomHeader(this.SINGLE_CT_FILE);
            dicomObj.dicomHeader.Modality = 'TEST_MODALITY';
            verifyError(this, @()CtSlice(dicomObj), 'CtSlice:constructor:InvalidInput');
        end
        
        function scaledImageNoDataTest(this)
            ctSlice = CtSlice(DicomObj(this.SINGLE_CT_FILE));
            verifyError(this, @()mean(ctSlice.scaledImageData(:)), 'CtSlice:get:scaledImageData');
        end
    end
end