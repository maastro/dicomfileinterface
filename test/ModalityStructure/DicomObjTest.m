classdef DicomObjTest < matlab.unittest.TestCase
    
    properties
        SINGLE_CT_FILE = './resources/maastro/ct/CT.9999.5421481024749737124441004800158416632.dcm';
        TEST_FILE_VR_ERROR = './resources/unittest/vrHeuristicsProblemCaseRtStruct.dcm';
        
        TAG_NAME = 'ROIContourSequence';
        TAG_THAT_IS_MISSING_BECAUSE_OF_ERROR = 'RTROIObservationsSequence';
        MEAN_OF_IMAGE = 286.6089;
    end
    
    methods (TestClassSetup)
    end
    
    methods (Test)
        %--- happy path ---%
        function dicominfoWrapperTest(this)
            dicomHeader = dicominfoWrapper(this.SINGLE_CT_FILE);
            dicomHeaderNative = dicominfo(this.SINGLE_CT_FILE);
            verifyEqual(this, dicomHeaderNative, dicomHeader);
        end
        
        function dicominfoWrapperVrHeuristicsTest(this)
            dicomHeader = dicominfoWrapper(this.TEST_FILE_VR_ERROR);
            tagThatShouldBeDifferent = dicomHeader.(this.TAG_NAME);
            dicomHeader = rmfield(dicomHeader, this.TAG_NAME);
            dicomHeader = rmfield(dicomHeader, this.TAG_THAT_IS_MISSING_BECAUSE_OF_ERROR);
            
            warning off; %#ok<WNOFF>
            dicomHeaderNative = dicominfo(this.TEST_FILE_VR_ERROR);
            warning on; %#ok<WNON>
            tagThatShouldBeDifferentNative = dicomHeaderNative.(this.TAG_NAME);
            dicomHeaderNative = rmfield(dicomHeaderNative, this.TAG_NAME);

            verifyEqual(this, dicomHeaderNative, dicomHeader);
            verifyNotEqual(this, tagThatShouldBeDifferentNative, tagThatShouldBeDifferent)
        end
        
        function readDicomHeaderTest(this)
            dicomObj = DicomObj();
            dicomObj = dicomObj.readDicomHeader(this.SINGLE_CT_FILE);
            verifyEqual(this, dicomObj.patientId, dicomObj.dicomHeader.PatientID);
        end
        
        function readDicomDataTest(this)
            dicomObj = DicomObj();
            dicomObj = dicomObj.readDicomHeader(this.SINGLE_CT_FILE);
            dicomObj = dicomObj.readDicomData();
            verifyEqual(this, mean(dicomObj.pixelData(:)), this.MEAN_OF_IMAGE, 'RelTol', 0.001);
        end
        
        function writeToFileTest(this)
            dicomObj = DicomObj();
            dicomObj = dicomObj.readDicomHeader(this.SINGLE_CT_FILE);
            dicomObj = dicomObj.readDicomData();
            dicomObj.writeToFile(cd);
            
            newDicomObj = DicomObj(fullfile(cd, [dicomObj.modality '_' dicomObj.sopInstanceUid '.dcm']));
            newDicomObj = newDicomObj.readDicomData();
            fields = fieldnames(dicomObj);
            fields(strcmp(fields, 'dicomHeader')) = [];
            fields(strcmp(fields, 'sopInstanceUid')) = [];
            fields(strcmp(fields, 'filename')) = [];
            for i = 1:length(fields)
                verifyEqual(this, newDicomObj.(fields{i}), dicomObj.(fields{i}));
            end
            
            if exist([dicomObj.modality '_' dicomObj.sopInstanceUid '.dcm'], 'file')
                delete([dicomObj.modality '_' dicomObj.sopInstanceUid '.dcm']);
            end
        end
        
        function createModalityObjTest(this)
            dicomObj = DicomObj();
            dicomObj = dicomObj.readDicomHeader(this.SINGLE_CT_FILE);
            modalityObj = createModalityObj(dicomObj);
            verifyEqual(this, isa(modalityObj, 'CtSlice'), true);
        end
        
        function modalityToDicomObjTest(this)
            modalityObj = CtSlice();
            modalityObj = modalityObj.readDicomHeader(this.SINGLE_CT_FILE);
            dicomObj = modalityToDicomObj(modalityObj); 
            verifyEqual(this, isa(dicomObj, 'DicomObj'), true);
            
            directObj = DicomObj(this.SINGLE_CT_FILE);
            verifyEqual(this, dicomObj, directObj);
        end
        
        %--- unhappy path ---%
        function readDicomHeaderInputErrorTest(this)
            dicomObj = DicomObj();
            verifyError(this, @()dicomObj.readDicomHeader(1), 'DicomObj:readDicomHeader:InvalidInput')
            verifyError(this, @()dicomObj.readDicomHeader('anyFile'), 'DicomObj:readDicomHeader:DataNotAvailable')
            verifyError(this, @()dicomObj.readDicomHeader('readme.md'), 'DicomObj:readDicomHeader:DataNotAvailable')
            verifyError(this, @()this.setDicomHeaderTestHelper(dicomObj), 'DicomObj:set:dicomHeader:InvalidInput')
        end
        
        function readDicomDataFileNotLongerAvailableTest(this)
            localTestFile = fullfile(cd, 'test.dcm');
            copyfile(this.SINGLE_CT_FILE, localTestFile)
            dicomObj = DicomObj(localTestFile);
            delete(localTestFile);
            verifyError(this, @()dicomObj.readDicomData(), 'DicomObj:readDicomDataOverwrite:ProcessingError')
        end
        
        function writeToFileInvalidInputTest(this)
            dicomObj = DicomObj();
            dicomObj = dicomObj.readDicomHeader(this.SINGLE_CT_FILE);
            dicomObj = dicomObj.readDicomData();
            verifyError(this, @()dicomObj.writeToFile('./nonexistingdir'), 'DicomObj:writeToFile:InvalidInput');
        end
    end
    
    methods
        function setDicomHeaderTestHelper(~, dicomObj)
            dicomObj.dicomHeader = 'someInput';
        end
    end
end