classdef ImageTest < matlab.unittest.TestCase
    
    properties
        RTSTRUCT_FILE = './resources/maastro/RTSTRUCT.9999.70989091007225463773458855903144372742.dcm';
        RTDOSE_FILE = './resources/maastro/RTDOSE.9999.245420512207561792050922341714899059838.dcm';
        TEST_CT_SCAN_DIR = './resources/maastro/ct/'
        ROI_NAME = 'GTV-1';
        
        contour
        image
        imageDose
        
        REF_VOLUME_CONTOUR = 58.2733;
        REF_VOLUME_FIRST_7_SLICES_CONTOUR = 0;
        REF_VOLUME_BOTH_CONTOURS = 237.4134;
        REF_MEAN_DOSE = 48.3739;
        REF_STD_DOSE = 0.8195;
        REF_PIXEL_DOSE = 48.8414;
        REF_PIXEL_INDEX_X = 30;
        REF_PIXEL_INDEX_Y = 10;
        REF_PIXEL_INDEX_Z = 30;
    end
    
    methods (TestClassSetup)
        function setupOnce(this)
            rtStruct = RtStruct(DicomObj(this.RTSTRUCT_FILE));
            this.contour = createContour(rtStruct, this.ROI_NAME);
            this.image = createImageFromCtScan(CtScan(this.TEST_CT_SCAN_DIR));
            rtDose = RtDose(DicomObj(this.RTDOSE_FILE));
            rtDose = rtDose.readDicomData();
            this.imageDose = createImageFromRtDose(rtDose);
        end
    end
    
    methods (Test)
        % --- happy path ---%
        function this = createVolumeOfInterestTest(this)
            voi = createVolumeOfInterest(this.contour, this.image);
            verifyEqual(this, voi.volume, this.REF_VOLUME_CONTOUR, 'RelTol', 0.001);
        end
        
        function this = createImageDataForVoiTest(this)
            voi = createVolumeOfInterest(this.contour, this.image);
            voiImage = createImageDataForVoi(voi, matchImageRepresentation(this.imageDose, this.image));
            verifyEqual(this, mean(voiImage.pixelData(~isnan(voiImage.pixelData(:)))), this.REF_MEAN_DOSE, 'RelTol', 0.001);
            verifyEqual(this, std(voiImage.pixelData(~isnan(voiImage.pixelData(:)))), this.REF_STD_DOSE, 'RelTol', 0.001);
            verifyEqual(this, voiImage.pixelData(this.REF_PIXEL_INDEX_X, this.REF_PIXEL_INDEX_Y, this.REF_PIXEL_INDEX_Z), ....
                    this.REF_PIXEL_DOSE, 'RelTol', 0.001);
        end
        
        function this = createImageDataForVoiFullGridTest(this)
            voi = createVolumeOfInterest(this.contour, this.image);
            refMatchedDoseImage = matchImageRepresentation(this.imageDose, this.image);
            voiImage = createImageDataForVoiFullGrid(voi, refMatchedDoseImage);
            verifyEqual(this, mean(voiImage.pixelData(~isnan(voiImage.pixelData(:)))), this.REF_MEAN_DOSE, 'RelTol', 0.001);
            verifyEqual(this, std(voiImage.pixelData(~isnan(voiImage.pixelData(:)))), this.REF_STD_DOSE, 'RelTol', 0.001);
            verifyEqual(this, voi.realX, refMatchedDoseImage.realX, 'RelTol', 0.001);
            verifyEqual(this, voi.realY, refMatchedDoseImage.realY, 'RelTol', 0.001);
            verifyEqual(this, voi.realZ, refMatchedDoseImage.realZ, 'RelTol', 0.001);
        end
        
        function this = addVoisTest(this)
            voi = createVolumeOfInterest(this.contour, this.image);
            [firstHalfContour, secondHalfContour] = this.splitContour(this.contour);
            firstVoi = createVolumeOfInterest(firstHalfContour, this.image);
            secondVoi = createVolumeOfInterest(secondHalfContour, this.image);
            addedVois = firstVoi + secondVoi;
            verifyEqual(this, addedVois.volume, this.REF_VOLUME_CONTOUR, 'RelTol', 0.001);
            verifyEqual(this, addedVois.pixelData, voi.pixelData, 'RelTol', 0.001);
        end
        
        function this = subtractVoisTest(this)
            voi = createVolumeOfInterest(this.contour, this.image);
            [firstHalfContour, secondHalfContour] = this.splitContour(this.contour);
            firstVoi = createVolumeOfInterest(firstHalfContour, this.image);
            secondVoi = createVolumeOfInterest(secondHalfContour, this.image);
            subtractedVois = voi - secondVoi;
            verifyEqual(this, subtractedVois.volume, firstVoi.volume, 'RelTol', 0.001);
            verifyEqual(this, subtractedVois.pixelData, firstVoi.pixelData, 'RelTol', 0.001);
        end
        
        function this = calculateImageStatisticsTest(this)
            out = calculateImageStatistics(this.imageDose, 'min');
            assertNotEmpty(this, out);
            out = calculateImageStatistics(this.imageDose, 'mean');
            assertNotEmpty(this, out);
            out = calculateImageStatistics(this.imageDose, 'median');
            assertNotEmpty(this, out);
            out = calculateImageStatistics(this.imageDose, 'max');
            assertNotEmpty(this, out);
            out = calculateImageStatistics(this.imageDose, 'sum');
            assertNotEmpty(this, out);
            out = calculateImageStatistics(this.imageDose, 'std');
            assertNotEmpty(this, out);
        end
        
        %--- unhappy path ---%
        function imageAddWrongPixelDataTest(this)
            verifyError(this, @()this.image.addPixelData(zeros(10,10,10)), 'Image:addPixelData:InvalidInput');
        end
        
        function combineWrongDimensionsVoi(this)
            voi = createVolumeOfInterest(this.contour, this.image);
            voi2 = VolumeOfInterest();
            verifyError(this, @()voi+voi2, 'VolumeOfInterest:plus:InvalidInput');
            verifyError(this, @()voi-voi2, 'VolumeOfInterest:minus:InvalidInput');
        end
        
        function  createVolumeOfInterestInvalidInputTest(this)
           verifyError(this, @()createImageDataForVoi(VolumeOfInterest, this.imageDose), 'createImageDataForVoi:InvalidInput');
        end
        
        function calculateImageStatisticsInvalidInputTest(this)
            verifyError(this, @()calculateImageStatistics(double(1), 'min'), 'calculateImageStatistics:InvalidInput');
            verifyError(this, @()calculateImageStatistics(Image, 'min'), 'calculateImageStatistics:InvalidInput');
            verifyError(this, @()calculateImageStatistics(this.imageDose, 'invalidOperation'), 'calculateImageStatistics:InvalidInput');
        end
        
    end
    
    methods 
        function [first, second] = splitContour(this, contour)
            dicomHeaderFirst = contour.dicomHeader;
            dicomHeaderFirst.ROIName = [this.ROI_NAME '_FIRST_HALF'];
            dicomHeaderSecond = contour.dicomHeader;
            dicomHeaderSecond.ROIName = [this.ROI_NAME '_SECOND_HALF'];
            fieldNames = fieldnames(dicomHeaderFirst.ContourSequence);
            for i = 1:length(fieldNames)
                if i < round(length(fieldNames)/2)
                    dicomHeaderSecond.ContourSequence = rmfield(dicomHeaderSecond.ContourSequence, fieldNames{i});
                else
                    dicomHeaderFirst.ContourSequence = rmfield(dicomHeaderFirst.ContourSequence, fieldNames{i});
                end
            end
            first = Contour(dicomHeaderFirst);
            second = Contour(dicomHeaderSecond);
        end
    end
end