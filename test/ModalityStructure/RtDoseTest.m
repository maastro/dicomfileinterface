classdef RtDoseTest < matlab.unittest.TestCase
    %RTDOSETEST Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        RTDOSE_FILE = './resources/maastro/RTDOSE.9999.245420512207561792050922341714899059838.dcm';
        RTDOSE_RELATIVE_FILE = './resources/unittest/relativeRtDoseTest.dcm'
        REF_MEAN_SCALED_IMAGE = 2.540829278;
        REF_MAX_SCALED_IMAGE = 50.75918941;
        PIXEL_COORDINATES_X = 94;
        PIXEL_COORDINATES_Y = 70;
        PIXEL_COORDINATES_Z = 51;
        PIXEL_COORDINATES_F = 1;
        REF_VALUE_AT_PIXEL_COORDINATES = 46.42923172;
        PLAN_TARGET_PRESCRIPTIONDOSE = 45;
    end
    
    methods (Test)
        % --- happy path ---%
        function rtDoseTest(this)
            rtDose = RtDose(DicomObj(this.RTDOSE_FILE));
            verifyEqual(this, rtDose.numberOfFrames, 136);
            verifyEqual(this, rtDose.is3dDose, true);
        end
        
        function scaledImageTest(this)
            rtDose = RtDose(DicomObj(this.RTDOSE_FILE));
            rtDose = rtDose.readDicomData();
            verifyEqual(this, mean(rtDose.scaledImageData(:)), this.REF_MEAN_SCALED_IMAGE, 'RelTol', 0.001);
            verifyEqual(this, max(rtDose.scaledImageData(:)), this.REF_MAX_SCALED_IMAGE, 'RelTol', 0.001);
            verifyEqual(this, rtDose.scaledImageData(this.PIXEL_COORDINATES_X, this.PIXEL_COORDINATES_Y, this.PIXEL_COORDINATES_F, this.PIXEL_COORDINATES_Z),...
                this.REF_VALUE_AT_PIXEL_COORDINATES, 'RelTol', 0.001);
        end
        
        function combineDosePixelDataTest(this)
            toCombineRtDoses = RtDose(DicomObj(this.RTDOSE_FILE));
            toCombineRtDoses = toCombineRtDoses.readDicomData();
            toCombineRtDoses(2) = toCombineRtDoses(1);
            [binData, sumDoseGridScaling] = combineDosePixelData(toCombineRtDoses);
            
            rtDose =  RtDose(DicomObj(this.RTDOSE_FILE));
            rtDose.dicomHeader.DoseGridScaling = sumDoseGridScaling;
            binData(:,:,:,:) = binData(end:-1:1,:,:,:);
            binData= permute(binData,[2 4 3 1]);
            rtDose.pixelData = binData;
            
            verifyEqual(this, mean(rtDose.scaledImageData(:)), this.REF_MEAN_SCALED_IMAGE * 2, 'RelTol', 0.001);
            verifyEqual(this, max(rtDose.scaledImageData(:)), this.REF_MAX_SCALED_IMAGE * 2, 'RelTol', 0.001);
            verifyEqual(this, rtDose.scaledImageData(this.PIXEL_COORDINATES_X, this.PIXEL_COORDINATES_Y, this.PIXEL_COORDINATES_F, this.PIXEL_COORDINATES_Z),...
                this.REF_VALUE_AT_PIXEL_COORDINATES * 2, 'RelTol', 0.001);
        end
        
        function createImageFromRtDoseTest(this)
            rtDose = RtDose(DicomObj(this.RTDOSE_FILE));
            rtDose = rtDose.readDicomData();
            image = createImageFromRtDose(rtDose);
            
            verifyEqual(this, mean(image.pixelData(:)), this.REF_MEAN_SCALED_IMAGE, 'RelTol', 0.001);
            verifyEqual(this, max(image.pixelData(:)), this.REF_MAX_SCALED_IMAGE, 'RelTol', 0.001);
            verifyEqual(this, image.pixelData(this.PIXEL_COORDINATES_X, this.PIXEL_COORDINATES_Y, this.PIXEL_COORDINATES_Z),...
                this.REF_VALUE_AT_PIXEL_COORDINATES, 'RelTol', 0.001);
        end
        
        function createImageFromRelativeRtDoseTest(this)
            rtDose = RtDose(DicomObj(this.RTDOSE_RELATIVE_FILE));
            rtDose = rtDose.readDicomData();
            image = createImageFromRelativeRtDose(rtDose, this.PLAN_TARGET_PRESCRIPTIONDOSE);
            
            verifyEqual(this, mean(image.pixelData(:)), this.REF_MEAN_SCALED_IMAGE, 'RelTol', 0.001);
            verifyEqual(this, max(image.pixelData(:)), this.REF_MAX_SCALED_IMAGE, 'RelTol', 0.001);
            verifyEqual(this, image.pixelData(this.PIXEL_COORDINATES_X, this.PIXEL_COORDINATES_Y, this.PIXEL_COORDINATES_Z),...
                this.REF_VALUE_AT_PIXEL_COORDINATES, 'RelTol', 0.001);
        end
        
        %--- unhappy path ---%
        function rtDoseInvalidInputTest(this)
            dicomObj = DicomObj();
            verifyError(this, @()RtDose(dicomObj), 'RtDose:constructor:InvalidInput');
            
            dicomObj = dicomObj.readDicomHeader(this.RTDOSE_FILE);
            dicomObj.dicomHeader.Modality = 'TEST_MODALITY';
            verifyError(this, @()RtDose(dicomObj), 'RtDose:constructor:InvalidInput');
        end
        
        function createImageFromRtDoseInvalidInputTest(this)
            rtdose = RtDose(DicomObj(this.RTDOSE_FILE));
            verifyError(this, @()createImageFromRtDose(rtdose), 'createImageFromRtDose:InvalidInput');
            rtdose.pixelData(3,3,3) = 4; 
            rtdose.dicomHeader.ImageOrientationPatient = [-1;0;0;0;1;0];
            verifyError(this, @()createImageFromRtDose(rtdose), 'createImageFromRtDose:ProcessingError');
            
            rtdoseRelative = RtDose(DicomObj(this.RTDOSE_RELATIVE_FILE));
            rtdoseRelative = rtdoseRelative.readDicomData();
            verifyError(this, @()createImageFromRtDose(rtdoseRelative), 'createImageFromRtDose:InvalidInput');
        end
        
        function combineDosePixelDataInvalidInputTest(this)
            rtDose = RtDose(DicomObj(this.RTDOSE_FILE));
            
            otherDose = rtDose;
            otherDose.dicomHeader.FrameOfReferenceUID = dicomuid;
            verifyError(this, @()combineDosePixelData([rtDose otherDose]), 'combineDosePixelData:InvalidInput');
            
            otherDose = rtDose;
            otherDose.dicomHeader.ImageOrientationPatient(1) = 0.5;
            verifyError(this, @()combineDosePixelData([rtDose otherDose]), 'combineDosePixelData:InvalidInput');
            
            otherDose = RtDose(DicomObj(this.RTDOSE_RELATIVE_FILE));
            verifyError(this, @()combineDosePixelData([rtDose otherDose]), 'combineDosePixelData:InvalidInput');
        end
    end
    
end

