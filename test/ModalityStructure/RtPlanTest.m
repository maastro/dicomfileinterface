classdef RtPlanTest < matlab.unittest.TestCase
    
    properties
        RTPLAN_FILE = './resources/maastro/RTPLAN.9999.42722492099431225192788770082885116854.dcm';
    end
    
    methods (TestClassSetup)

    end
    
    methods (Test)
        % --- happy path ---%
        function rtPlanTest(this)
            rtPlan = RtPlan(DicomObj(this.RTPLAN_FILE));
            verifyEqual(this, rtPlan.modality, 'rtplan');
        end
        
        %--- unhappy path ---%
        function  rtPlanInvalidInputTest(this)
            dicomObj = DicomObj();
            verifyError(this, @()RtPlan(dicomObj), 'RtPlan:constructor:InvalidInput');
            
            dicomObj = dicomObj.readDicomHeader(this.RTPLAN_FILE);
            dicomObj.dicomHeader.Modality = 'TEST_MODALITY';
            verifyError(this, @()RtPlan(dicomObj), 'RtPlan:constructor:InvalidInput');
        end
    end
end