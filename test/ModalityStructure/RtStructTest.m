classdef RtStructTest < matlab.unittest.TestCase
    
    properties
        RTSTRUCT_FILE = './resources/maastro/RTSTRUCT.9999.70989091007225463773458855903144372742.dcm';
        ROI_NAME = 'GTV-1';
        ROI_NUMBER = 7;
        rtStruct;
    end
    
    methods (TestClassSetup)
        function setupOnce(this) %reading the rtstruct takes to long, put in setup
            this.rtStruct = RtStruct(DicomObj(this.RTSTRUCT_FILE));
        end
    end
    
    methods (Test)
        % --- happy path ---%
        function this = rtPlanTest(this)
            verifyEqual(this, this.rtStruct.modality, 'rtstruct');
        end
        
        function dicomHeaderForRoiNameTest(this)
            structHeader = dicomHeaderForRoiName(this.rtStruct, this.ROI_NAME);
            verifyEqual(this, structHeader.ROINumber, this.ROI_NUMBER);
        end
        
        function dicomHeaderForRoiNumberTest(this)
            structHeader = dicomHeaderForRoiNumber(this.rtStruct, this.ROI_NUMBER);
            verifyEqual(this, structHeader.ROIName, this.ROI_NAME);
        end
        
        %--- unhappy path ---%
        function  rtPlanInvalidInputTest(this)
            dicomObj = DicomObj();
            verifyError(this, @()RtStruct(dicomObj), 'RtStruct:constructor:InvalidInput');
            
            dicomObj = dicomObj.readDicomHeader(this.RTSTRUCT_FILE);
            dicomObj.dicomHeader.Modality = 'TEST_MODALITY';
            verifyError(this, @()RtStruct(dicomObj), 'RtStruct:constructor:InvalidInput');
        end
        
        function dicomHeaderForRoiNameInvalidNameTest(this)
            verifyError(this, @()dicomHeaderForRoiName(this.rtStruct, 'NotAValidName'), 'dicomHeaderForRoiName:DataNotAvailable');
        end
        
        function dicomHeaderForRoiNumberInvalidNumberTest(this)
            verifyError(this, @()dicomHeaderForRoiNumber(this.rtStruct, -1), 'dicomHeaderForRoiNumber:DataNotAvailable');
        end
    end
end