classdef VolumeOfInterestMathematicsTest < matlab.unittest.TestCase
    
    
    properties
        VOLUME_SPHERE = (4/3)*pi*(5^3);
        RELATIVE_ERROR = 0.005;
        REFERENCE_IMAGE
    end

    methods (TestClassSetup)
        function setupOnce(this)            
            PIXELSPACING_Y = 0.3; %values in cm
            PIXELSPACING_X_Z = 0.1;
            START_AXIS_Y = -22.05;
            END_AXIS_Y = 22.05;
            START_AXIS_X_Z = -25;
            END_AXIS_X_Z = 25;
            
            this.REFERENCE_IMAGE = Image(PIXELSPACING_X_Z, ...
                                    PIXELSPACING_Y, ...
                                    PIXELSPACING_X_Z, ...
                                    START_AXIS_X_Z:PIXELSPACING_X_Z:END_AXIS_X_Z, ...
                                    START_AXIS_Y:PIXELSPACING_Y:END_AXIS_Y, ...
                                    START_AXIS_X_Z:PIXELSPACING_X_Z:END_AXIS_X_Z, []);
        end
        
        function teardownOnce(~) %leave this here for when i might need it.  
        end
    end
    
    methods (Test)
        function testSameGrid(this)
            sphere = mockSphereContour();
            voi = createVolumeOfInterest(sphere, this.REFERENCE_IMAGE);
            verifyEqual(this, voi.volume, this.VOLUME_SPHERE, 'RelTol', this.RELATIVE_ERROR)
        end
        
        function testShiftedGrid(this)
            newRef = this.REFERENCE_IMAGE;
            newRef.realY = newRef.realY + newRef.pixelSpacingY*0.4;
            sphere = mockSphereContour();
            voi = createVolumeOfInterest(sphere,newRef);
            verifyEqual(this, voi.volume, this.VOLUME_SPHERE, 'RelTol', this.RELATIVE_ERROR)
            
            newRef = this.REFERENCE_IMAGE;
            newRef.realY = newRef.realY + newRef.pixelSpacingY*0.5;
            voi = createVolumeOfInterest(sphere,newRef);
            verifyEqual(this, voi.volume, this.VOLUME_SPHERE, 'RelTol', 0.015)
            %this test will fail, this is the limit of the algorithm, if i put the this.REFERENCE_IMAGEerence image in the
            %middle of where the contours are delineated the volume will be off +/-1%
            %i do not know if have to keep this in, but i want to document somehow that this is the
            %limit of the algorithm!
        end
        
        function testDouleGrid(this)
            newRef = this.REFERENCE_IMAGE;
            newRef.pixelSpacingY = newRef.pixelSpacingY/2;
            newRef.realY = newRef.realY(1):newRef.pixelSpacingY:newRef.realY(end);
            newRef.pixelSpacingX = newRef.pixelSpacingX/2;
            newRef.realX = newRef.realX(1):newRef.pixelSpacingX:newRef.realX(end);
            newRef.pixelSpacingZ = newRef.pixelSpacingZ/2;
            newRef.realZ = newRef.realZ(1):newRef.pixelSpacingZ:newRef.realZ(end);
            sphere = mockSphereContour();
            voi = createVolumeOfInterest(sphere,newRef);
            verifyEqual(this, voi.volume, this.VOLUME_SPHERE, 'RelTol', this.RELATIVE_ERROR)
        end
        
        function testHalfGrid(this)
            newRef = this.REFERENCE_IMAGE;
            newRef.realY = newRef.realY(1:2:end);
            newRef.pixelSpacingY = newRef.pixelSpacingY*2;
            newRef.realX = newRef.realX(1:2:end);
            newRef.pixelSpacingX = newRef.pixelSpacingX*2;
            newRef.realZ = newRef.realZ(1:2:end);
            newRef.pixelSpacingZ = newRef.pixelSpacingZ*2;
            sphere = mockSphereContour();
            voi = createVolumeOfInterest(sphere,newRef);
            verifyEqual(this, voi.volume, this.VOLUME_SPHERE, 'RelTol', this.RELATIVE_ERROR)
        end
        
        function testWithMoreContoursInSlice(this)
            sphere = mockDoubleSphereContourInZaxis();
            voi = createVolumeOfInterest(sphere,this.REFERENCE_IMAGE);
            verifyEqual(this, voi.volume, this.VOLUME_SPHERE*2, 'RelTol', this.RELATIVE_ERROR)
        end
        
        function testWithDistanceInLengthAxis(this)
            sphere = mockDoubleSphereContourInXYaxis();
            voi = createVolumeOfInterest(sphere,this.REFERENCE_IMAGE);
            verifyEqual(this, voi.volume, this.VOLUME_SPHERE*2, 'RelTol', this.RELATIVE_ERROR)
        end
    end
end