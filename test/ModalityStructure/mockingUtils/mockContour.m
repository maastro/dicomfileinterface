function contour = mockContour(xyzCellArray)
    for i = 1:length(xyzCellArray)
        dicomHeader.ContourSequence.(['Item_' num2str(i)]).ContourImageSequence.Item_1.ReferencedSOPClassUID = '';
        dicomHeader.ContourSequence.(['Item_' num2str(i)]).ContourImageSequence.Item_1.ReferencedSOPInstanceUID = '';
        sequencedCoordinates = [];
        for j = 1:length(xyzCellArray{i})
            sequencedCoordinates = [sequencedCoordinates; ...
                                    xyzCellArray{i}(j,1)*10; ...
                                    xyzCellArray{i}(j,2)*10; ...
                                    xyzCellArray{i}(j,3)*-10]; %#ok<AGROW>
        end
        dicomHeader.ContourSequence.(['Item_' num2str(i)]).ContourData = sequencedCoordinates;
        dicomHeader.ContourSequence.(['Item_' num2str(i)]).ContourGeometricType = 'CLOSED_PLANAR';
    end
    contour = Contour(dicomHeader);
end

