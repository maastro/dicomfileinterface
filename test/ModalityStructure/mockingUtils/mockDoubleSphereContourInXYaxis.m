function sphere = mockDoubleSphereContourInXYaxis()
    z = -4.95:0.3:4.95; %3mm grid to mock the CT grid.
    iCircle = [17:-1:1, 1:1:17];
    x = cell(34,1); 
    y = cell(34,1);
    
    for i = 0.5:1:16.5
       scalingFactor = sqrt(25-((i*0.3)^2));
       [x{i+0.5},y{i+0.5}] = pol2cart(linspace(0,2*pi,200)', scalingFactor);
    end
    
    coordinates = cell(68,1);
    for i = 1:68
        if mod(i,2) == 0
            coordinates{i}(:,1) = x{iCircle(ceil(i/2))}+10;
        else
            coordinates{i}(:,1) = x{iCircle(ceil(i/2))}-10;
        end
        coordinates{i}(:,2) = y{iCircle(ceil(i/2))};
        coordinates{i}(1:length(x{1}),3) = z(ceil(i/2));
    end

    sphere = mockContour(coordinates);
end