function sphere = mockDoubleSphereContourInZaxis()
    
    z = [-12.15:0.3:-2.25, 2.25:0.3:12.15]; %3mm grid to mock the CT grid.
    iCircle = [17:-1:1, 1:17, 17:-1:1, 1:17];
    
    x = cell(17,1); y = cell(17,1);
    for i = 0.5:1:16.5
       scalingFactor = sqrt(25-((i*0.3)^2));
       [x{i+0.5},y{i+0.5}] = pol2cart(linspace(0,2*pi,200)', scalingFactor);
    end
    
    coordinates = cell(68,1);
    for i = 1:34
        coordinates{i}(:,1) = x{iCircle(i)};
        coordinates{i}(:,2) = y{iCircle(i)};
        coordinates{i}(1:length(x{1}),3) = z(i);
    end

    for i = 35:68
        coordinates{i}(:,1) = x{iCircle(i)};
        coordinates{i}(:,2) = y{iCircle(i)};
        coordinates{i}(1:length(x{1}),3) = z(i);
    end

    sphere = mockContour(coordinates);
end