function results = modalityStructureTest()
logger = logging.getLogger('dicom-file-interface');
logger.setCommandWindowLevel(logger.OFF);

test = DicomObjTest();
results = test.run;
clear test;

test = ContourSliceTest();
results = [results test.run];
clear test;

test = CtSliceTest();
results = [results test.run];
clear test;

test = RtDoseTest();
results = [results test.run];
clear test;

test = RtPlanTest();
results = [results test.run];
clear test;

test = RtStructTest();
results = [results test.run];
clear test;

test = ContourTest();
results = [results test.run];
clear test;

test = CtScanTest();
results = [results test.run];
clear test;

test = ImageTest();
results = [results test.run];
clear test;

test = VolumeOfInterestMathematicsTest;
results = [results test.run];
clear test;
end