function path = findResourcesFolder()
    found = false;
    home = cd;
    count = 0;
%     logger = MatlabLogger.getLogger();
%     logger.debug(logger.STATUS_RUNNING, '#UIDPLACEHOLDER#', ['start looking for ./resources/ from ' strrep(cd, '\', '/')]);
    
    while ~found
        dirResult = dir('resources');
        
        if isempty(dirResult)
           cd ..;
           count = count + 1;
        else
            found = true;
            path = cd;
            path = [strrep(path, '\', '/') '/resources'];
            cd(home);
%             logger.info(logger.STATUS_RUNNING, '#UIDPLACEHOLDER#', ['resource folder found ' path]);
        end
        
        if count == 20
            cd(home);
            message = 'Could not find resource folder after 20 attempts, i quit!';
%             logger.error(logger.STATUS_CALCULATIONERROR, '#UIDPLACEHOLDER#', message);
            throw(MException('MATLAB:findResourcesFolder:FolderNotFound', message));            
        end
    end
end