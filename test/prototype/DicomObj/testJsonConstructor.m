function testJsonConstructor(this)
    dicomObj = DicomObj(this.TEST_FILE);
    dicomObj = dicomObj.readDicomData();
    [~, json] = dicomObj.toJsonFile(fullfile(findResourcesFolder(), 'unittest'));
    dicomObjFromJson = DicomObj();
    dicomObjFromJson = dicomObjFromJson.loadFromJson(json);  
    verifyEqualDicomObj(this, dicomObjFromJson, dicomObj, fieldnames(dicomObj))
    verifyEqualDicomObj(this, createModalityObj(dicomObjFromJson), createModalityObj(dicomObj), fieldnames(createModalityObj(dicomObj)))
end

function verifyEqualDicomObj(this, struct, refStuct, fields)
    if isempty(fields)
        return;
    end
    
    if isnumeric(refStuct.(fields{1}))
        verifyEqual(this, double(struct.(fields{1})), double(refStuct.(fields{1})));
    elseif strcmp('', refStuct.(fields{1}))
        verifyEqual(this, isempty(struct.(fields{1})), isempty(refStuct.(fields{1})));
    elseif ischar(refStuct.(fields{1}))
        verifyEqual(this, struct.(fields{1}), refStuct.(fields{1}));
    end
    
    fields(1) = [];
    verifyEqualDicomObj(this, struct, refStuct, fields);
end